package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.n3.{NamedResource, NsName}
import scala.util.Try
import scala.collection.mutable
import com.trustlayers.reasoner.n3.N3Parser.{Node, Properties, Property}
import scala.util.Failure

abstract class AirRule(name: NsName) extends NamedResource {
  var label = ""
  var comment = ""
  var ifClause: Node = EmptyNode
  val thenClauses = mutable.ListBuffer.empty[Node]
  val elseClauses = mutable.ListBuffer.empty[Node]

  def setIfClause(clause: Node) = {
    ifClause = clause
  }

  def addThenClause(clause: Node) {
    thenClauses += clause
  }

  def addElseClause(clause: Node) {
    elseClauses += clause
  }

  def processNestedRules() {
    def processNestedRules(clause: Node): Try[Node] =
      clause match {
        case Properties(Property(AirRuleRelation, pp: Properties) :: Nil) => ruleFromProperties(pp)
        case _ => Try(clause)
      }

    val thenList = thenClauses.toList
    thenClauses.clear()
    thenClauses ++= thenList.map(processNestedRules).map(_.get)

    val elseList = elseClauses.toList
    elseClauses.clear()
    elseClauses ++= elseList.map(processNestedRules).map(_.get)
  }

  def ruleFromProperties(pp: Properties): Try[AirRule] = {
    // TODO arbitrarily nested rules, including rules invoked by name
    // check if we have an air:if property
    if (pp.list.exists(_.pred == AirIf)) {
      Try {
        val nestedRule = BeliefRule(emptyNsName)
        pp.list.foreach {
          case Property(AirIf, clause) => nestedRule.setIfClause(clause)
          case Property(AirThen, clause) => nestedRule.addThenClause(clause)
          case Property(AirElse, clause) => nestedRule.addElseClause(clause)
          case x => throw new Exception("unknown property " + x)
        }
        nestedRule
      }
    } else
      Failure(new Exception("bad syntax for nested rule " + pp))
  }
}