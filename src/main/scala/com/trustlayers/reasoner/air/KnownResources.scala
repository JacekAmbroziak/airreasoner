package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.n3.NamedResource
import com.trustlayers.reasoner.n3.N3Parser.Node

trait KnownResources {
  private val known = toMap(List(IsA, RdfsSeeAlso, RdfsComment, RdfsLabel))

  def toMap(resources: List[NamedResource]) = resources.map(r => (r.name, r)).toMap[Node, NamedResource]

  def maybeKnown(x: Node) = known.getOrElse(x, x)
}
