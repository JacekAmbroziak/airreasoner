package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.n3.N3Parser.Node

trait AirResources extends KnownResources {
  private val known = toMap(List(AirPolicyResource, AirRuleSetResource,
    AirRuleRelation,
    AirBeliefRuleResource, AirHiddenRuleResource, AirElidedRuleResource,
    AirIf, AirThen, AirElse,
    AirDescription, AirComment,
    AirJustifies,
    AirAssert, AirCompliantWith, AirNonCompliantWith))

  override def maybeKnown(x: Node) = known.getOrElse(x, super.maybeKnown(x))
}
