package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.n3.NsName

/*
 * Only the air:Description is provided for an elided rule.
 */
case class ElidedRule(name: NsName) extends AirRule(name) {
  override def toString: String =
    s"ElidedRule($name)" + "\n\tIF:\t" + ifClause + "\n\tTHEN:\t" + thenClauses + "\n\tELSE:\t" + elseClauses
}
