package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.n3.NsName

/*
 * All justifications for a hidden rule are suppressed.
 */
case class HiddenRule(name: NsName) extends AirRule(name) {
  override def toString: String =
    s"HiddenRule($name)" + "\n\tIF:\t" + ifClause + "\n\tTHEN:\t" + thenClauses + "\n\tELSE:\t" + elseClauses
}
