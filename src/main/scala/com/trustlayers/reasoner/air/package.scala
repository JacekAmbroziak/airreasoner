package com.trustlayers.reasoner

import java.io.{StringReader, BufferedReader}
import com.trustlayers.reasoner.n3._
import com.trustlayers.reasoner.n3.NsName
import com.trustlayers.reasoner.n3.Resource
import com.trustlayers.reasoner.n3.MathOp
import com.trustlayers.reasoner.n3.Relation

package object air {
  val rdf_syntax_ns = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  val owl_ns = "http://www.w3.org/2002/07/owl#"
  val swap_log_ns = "http://www.w3.org/2000/10/swap/log#"
  val swap_math_ns = "http://www.w3.org/2000/10/swap/math#"
  val air_ns = "http://dig.csail.mit.edu/TAMI/2007/amord/air#"
  val rdf_schema_ns = "http://www.w3.org/2000/01/rdf-schema#"

  val emptyNsName = NsName("", "")

  object IsA extends Relation(NsName(rdf_syntax_ns, "type")) {
    override def toString: String = "IsA"
  }

  object RdfsLabel extends Relation(NsName(rdf_schema_ns, "label")) {
    override def toString: String = "rdfs:label"
  }

  object RdfsComment extends Relation(NsName(rdf_schema_ns, "comment")) {
    override def toString: String = "rdfs:comment"
  }

  object RdfsSeeAlso extends Relation(NsName(rdf_schema_ns, "seeAlso")) {
    override def toString: String = "rdfs:seeAlso"
  }

  object AirRuleRelation extends Relation(NsName(air_ns, "rule")) {
    override def toString: String = "air:rule"
  }

  object AirIf extends Relation(NsName(air_ns, "if")) {
    override def toString: String = "air:if"
  }

  object AirThen extends Relation(NsName(air_ns, "then")) {
    override def toString: String = "air:then"
  }

  object AirElse extends Relation(NsName(air_ns, "else")) {
    override def toString: String = "air:else"
  }

  object AirAssert extends Relation(NsName(air_ns, "assert")) {
    override def toString: String = "air:assert"
  }

  object AirCompliantWith extends Relation(NsName(air_ns, "compliant-with")) {
    override def toString: String = "air:compliant-with"
  }

  object AirNonCompliantWith extends Relation(NsName(air_ns, "non-compliant-with")) {
    override def toString: String = "air:non-compliant-with"
  }

  object AirDescription extends Relation(NsName(air_ns, "description")) {
    override def toString: String = "air:description"
  }

  object AirComment extends Relation(NsName(air_ns, "comment")) {
    override def toString: String = "air:comment"
  }

  object AirJustifies extends Relation(NsName(air_ns, "justifies")) {
    override def toString: String = "air:justifies"
  }

  object MathSum extends MathOp(NsName(swap_math_ns, "sum")) {
    override def toString: String = "math:sum"
  }

  object MathDifference extends MathOp(NsName(swap_math_ns, "difference")) {
    override def toString: String = "math:difference"
  }

  object MathGreaterThan extends MathComp(NsName(swap_math_ns, "greaterThan")) {
    override def toString: String = "math:greaterThan"
  }

  object MathLessThan extends MathComp(NsName(swap_math_ns, "lessThan")) {
    override def toString: String = "math:lessThan"
  }

  object MathEqualTo extends MathComp(NsName(swap_math_ns, "equalTo")) {
    override def toString: String = "math:equalTo"
  }

  object MathNotEqualTo extends MathComp(NsName(swap_math_ns, "notEqualTo")) {
    override def toString: String = "math:notEqualTo"
  }

  object LogSemantics extends Relation(NsName(swap_log_ns, "semantics")) {
    override def toString: String = "log:semantics"
  }

  object LogIncludes extends Relation(NsName(swap_log_ns, "includes")) {
    override def toString: String = "log:includes"
  }

  object AirPolicyResource extends Resource(NsName(air_ns, "Policy")) {
    override def toString: String = "air:Policy"
  }

  object AirRuleSetResource extends Resource(NsName(air_ns, "RuleSet")) {
    override def toString: String = "air:RuleSet"
  }

  object AirBeliefRuleResource extends Resource(NsName(air_ns, "Belief-rule")) {
    override def toString: String = "air:Belief-rule"
  }

  object AirHiddenRuleResource extends Resource(NsName(air_ns, "Hidden-rule")) {
    override def toString: String = "air:Hidden-rule"
  }

  object AirElidedRuleResource extends Resource(NsName(air_ns, "Elided-rule")) {
    override def toString: String = "air:Elided-rule"
  }

  def concat(lines: Seq[String]): String = {
    lines.foldLeft(new StringBuilder)(_.append(_).append("\n")).toString()
  }

  def buildReader(lines: Seq[String]): BufferedReader = {
    new BufferedReader(new StringReader(concat(lines)))
  }
}