package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.n3.N3Parser.Node

trait LogResources extends KnownResources {
  private val known = toMap(List(LogSemantics, LogIncludes))

  override def maybeKnown(x: Node) = known.getOrElse(x, super.maybeKnown(x))
}
