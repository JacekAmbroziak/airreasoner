package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.n3.N3Parser.Node

trait MathResources extends KnownResources {
  private val known = toMap(List(MathSum, MathDifference, MathGreaterThan, MathLessThan, MathEqualTo, MathNotEqualTo))

  override def maybeKnown(x: Node) = known.getOrElse(x, super.maybeKnown(x))
}
