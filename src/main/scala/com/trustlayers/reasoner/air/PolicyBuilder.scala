package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.n3._
import N3Parser._
import scala.collection.immutable.ListMap
import com.trustlayers.reasoner.n3.NsName
import com.trustlayers.reasoner.n3.N3Parser.Formula
import com.trustlayers.reasoner.n3.N3Parser.N3List
import com.trustlayers.reasoner.n3.N3Parser.StringLiteral
import com.trustlayers.reasoner.n3.N3Parser.UniversalVariables
import com.trustlayers.reasoner.n3.N3Parser.Statement
import com.trustlayers.reasoner.n3.N3Parser.Statements
import com.trustlayers.reasoner.n3.N3Parser.Properties
import com.trustlayers.reasoner.n3.UniversalVariable
import com.trustlayers.reasoner.n3.ExistentialVariable
import com.trustlayers.reasoner.n3.N3Parser.Property
import scala.util.{Try, Success, Failure}
import org.apache.log4j.Logger

/**
 * PolicyBuilder takes in a set of N3Parser TopLevel items (Statements, mostly) and transforms them into
 * the corresponding Air constructs.
 * Those objects have their internal structure built up here too.
 */
class PolicyBuilder(val resources: Map[NsName, NamedResource]) extends AllResources {

  def processTopLevel(element: TopLevel): Try[PolicyBuilder] =
    element match {
      case UniversalVariables(list) =>
        Success(PolicyBuilder(list.foldLeft(resources)((m, v) => m + (v.name -> UniversalVariable(v.name)))))

      case Statement(subject, IsA, AirPolicyResource) => createRuleSet(subject)

      case Statement(subject, IsA, AirRuleSetResource) => createRuleSet(subject)

      case Statement(subject, IsA, AirBeliefRuleResource) => createBeliefRule(subject)

      case Statement(subject, IsA, AirHiddenRuleResource) => createHiddenRule(subject)

      case Statement(subject, IsA, AirElidedRuleResource) => createElidedRule(subject)

      case Statement(containerName: NsName, AirRuleRelation, ruleName: NsName) => addRuleToContainer(containerName, ruleName)

      case Statement(name: NsName, AirIf, clause: Node) => addRuleIfClause(name, clause)

      case Statement(name: NsName, AirThen, clause: Node) => addRuleThenClause(name, clause)

      case Statement(name: NsName, AirElse, clause: Node) => addRuleElseClause(name, clause)

      case Statement(name: NsName, RdfsLabel, label: StringLiteral) => labelResource(name, label)

      case Statement(name: NsName, RdfsComment, comment: StringLiteral) => commentOnResource(name, comment)

      case Statement(subj, pred, obj) => processStatement(subj, pred, obj)

      case _ => Success(this)
    }

  private def processStatement(subj: Node, pred: Node, obj: Node): Try[PolicyBuilder] = {
    PolicyBuilder.logger.warn(s"ignored?: $subj -> $pred -> $obj")
    Success(this)
  }

  private def createRuleSet(subj: Node): Try[PolicyBuilder] =
    subj match {
      case name: NsName => withNewResource(AirRuleSet(name))
      case _ => Failure(new Exception("bad rule set name"))
    }

  private def addRuleToContainer(policyName: NsName, ruleName: NsName): Try[PolicyBuilder] =
    resources.get(policyName) match {
      case Some(ruleSet: AirRuleSet) => ruleSet.addRuleName(ruleName); Success(this)
      case Some(what: Any) => Failure(new Exception("not a policy: " + policyName))
      case None => Failure(new Exception("policy not found: " + policyName))
    }

  private def createBeliefRule(subj: Node): Try[PolicyBuilder] =
    subj match {
      case name: NsName => withNewResource(BeliefRule(name))
      case _ => Failure(new Exception("bad policy name"))
    }

  private def createHiddenRule(subj: Node): Try[PolicyBuilder] =
    subj match {
      case name: NsName => withNewResource(HiddenRule(name))
      case _ => Failure(new Exception("bad policy name"))
    }

  private def createElidedRule(subj: Node): Try[PolicyBuilder] =
    subj match {
      case name: NsName => withNewResource(ElidedRule(name))
      case _ => Failure(new Exception("bad policy name"))
    }

  // recursively visit AST tree rooted at clause replacing NsNames that name variables with Variable objects
  // note that the resources map can contain other entities beyond variables, such as rules
  private def resolveVars(clause: Node): Node =
    clause match {
      case _: NamedResource => clause
      case name: NsName => resources.get(name) match {
        case Some(v: UniversalVariable) => v
        case Some(v: ExistentialVariable) => v
        case _ => clause
      }
      case statement: Statement => resolveVars(statement)
      case property: Property => resolveVars(property)
      case Formula(vars, statements) =>
        vars match {
          case head :: tail =>
            val localScope = PolicyBuilder(resources ++ vars.map(v => (v.name, ExistentialVariable(v.name))))
            Formula(vars, Statements(statements.statements.map(localScope.resolveVars)))
          case Nil => Formula(vars, Statements(statements.statements.map(resolveVars)))
        }
      case Properties(list) => Properties(list.map(resolveVars))
      case Statements(list) => Statements(list.map(resolveVars))
      case N3List(list) => N3List(list.map(resolveVars))
      case _: Node => clause
    }

  private def resolveVars(s: Statement): Statement =
    Statement(resolveVars(s.subj), resolveVars(s.pred), resolveVars(s.obj))

  private def resolveVars(p: Property): Property =
    p match {
      case Property(AirRuleRelation, _: NsName) => p // don't resolve rule's name at this stage
      case Property(resource: NamedResource, obj) => Property(resource, resolveVars(obj))
      case _ => Property(resolveVars(p.pred), resolveVars(p.obj))
    }

  private def addRuleIfClause(name: NsName, clause: Node): Try[PolicyBuilder] =
    resources.get(name) match {
      case Some(rule: AirRule) => rule.setIfClause(resolveVars(clause)); Success(this)
      case Some(what: Any) => Failure(new Exception("not a rule " + name))
      case None => Failure(new Exception("resource not found " + name))
    }

  private def addRuleThenClause(name: NsName, clause: Node): Try[PolicyBuilder] =
    resources.get(name) match {
      case Some(rule: AirRule) => rule.addThenClause(resolveVars(clause)); Success(this)
      case Some(what: Any) => Failure(new Exception("not a rule " + name))
      case None => Failure(new Exception("resource not found " + name))
    }

  private def addRuleElseClause(name: NsName, clause: Node): Try[PolicyBuilder] =
    resources.get(name) match {
      case Some(rule: AirRule) => rule.addElseClause(resolveVars(clause)); Success(this)
      case Some(what: Any) => Failure(new Exception("not a rule " + name))
      case None => Failure(new Exception("resource not found " + name))
    }

  private def labelResource(name: NsName, label: StringLiteral): Try[PolicyBuilder] =
    resources.get(name) match {
      case Some(rule: AirRule) => rule.label = label.value; Success(this)
      case Some(ruleSet: AirRuleSet) => ruleSet.label = label.value; Success(this)
      case Some(what: Any) => Failure(new Exception("cannot label " + name))
      case None => Failure(new Exception("resource not found " + name))
    }

  private def commentOnResource(name: NsName, label: StringLiteral): Try[PolicyBuilder] =
    resources.get(name) match {
      case Some(rule: AirRule) => rule.comment = label.value; Success(this)
      case Some(ruleSet: AirRuleSet) => ruleSet.comment = label.value; Success(this)
      case Some(what: Any) => Failure(new Exception("cannot comment " + name))
      case None => Failure(new Exception("resource not found " + name))
    }

  private def withNewResource(res: NamedResource): Try[PolicyBuilder] = Success(PolicyBuilder(resources + (res.name -> res)))

  def parseNestedRules: PolicyBuilder = {
    resources.collect {
      case (_, rule: AirRule) => rule.processNestedRules()
    }
    this
  }
}

object PolicyBuilder {
  val logger = Logger.getLogger("com.trustlayers.reasoner.air.PolicyBuilder")

  def apply(resources: Map[NsName, NamedResource]) = new PolicyBuilder(resources)

  def apply(): PolicyBuilder = apply(ListMap.empty[NsName, NamedResource])
}