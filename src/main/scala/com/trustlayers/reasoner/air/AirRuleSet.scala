package com.trustlayers.reasoner.air

import scala.collection.mutable.ListBuffer
import com.trustlayers.reasoner.n3.{NamedResource, NsName}

case class AirRuleSet(name: NsName) extends NamedResource {
  var label = ""
  var comment = ""
  val ruleNames = ListBuffer.empty[NsName]

  def addRuleName(ruleName: NsName) {
    ruleNames += ruleName
  }
}
