package com.trustlayers.reasoner.air

import java.io.Reader
import scala.util.{Failure, Try}
import com.trustlayers.reasoner.n3.{N3Parser, NamedResource, NsName}

/**
 * This class takes in a text representation of an AIR program and produces a set of top level constructs
 * (Rules, Rulesets, variables... -- see N3Parser) which can then be turned into an actual AIR program we can
 * transform/optimize and eventually execute.
 * It also parses out any nested rules as well and adds them.
 *
 * What the N3Parser parser parses is N3 (not AIR)
 * N3 is a notation for triples/statements + a few more constructs
 * AIR rules can be encoded in N3 by using AIR specific resources (like air:rule, air:if, air:then, etc)
 * to express AIR rules as graphs of triples
 *
 * The task of extracting an AIR program from N3 text consists of 2 phases:
 * 1) parse N3 to a list of Statements and other constructs (variables, comments, etc)
 * 2) try to interpret these statements as constituents of AIR Policies and Rules
 * All rules need to be found including nested rules to arrive at a N3-free representation of an AIR program
 *
 * Please note that the process can fail: first when parsing N3 (bugs in N3),
 * and then when going through N3's triples (bugs in following the AIR conventions)
 * Therefore parsing and AIR program results are wrapped in Try and can be Successes or Failures.
 *
 */

object AirRulesProcessor {
  def extractAirResources(reader: Reader): Try[Map[NsName, NamedResource]] =
  // TODO: Add comments to help Scala neophytes understand how the code below works.
    N3Parser.parse(reader)
      .transform(
        topLevel => Try(topLevel.foldLeft(PolicyBuilder())(_.processTopLevel(_).get).parseNestedRules.resources),
        f => Failure(f))
}
