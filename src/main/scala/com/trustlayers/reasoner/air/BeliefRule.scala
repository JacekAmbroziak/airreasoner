package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.n3.NsName

/**
 * User: jacek
 * Date: 3/31/14
 */

case class BeliefRule(name: NsName) extends AirRule(name) {
  override def toString = s"BeliefRule($name)" + "\n\tIF:\t" + ifClause + "\n\tTHEN:\t" + thenClauses + "\n\tELSE:\t" + elseClauses
}
