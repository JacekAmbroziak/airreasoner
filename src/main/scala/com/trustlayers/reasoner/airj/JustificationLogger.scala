package com.trustlayers.reasoner.airj

import com.trustlayers.reasoner.exec._
import scala.collection.mutable

import scala.slick.driver.H2Driver.simple._

// TODO prune data on paths not leading to any derivations


class JustificationLogger {
  val databaseDef = Database.forURL("jdbc:h2:mem:test1", driver = "org.h2.Driver")

  val rads = mutable.ArrayBuffer.empty[RuleApplicationData]

  var ruleFiringSerial = 0

  class ClosureComputations(tag: Tag) extends Table[(Option[Int], String)](tag, "closure_computation") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    def * = (id.?, name)
  }

  val closureComputations = TableQuery[ClosureComputations]

  var closureCompNumber = 0

  def startClosureComputation(factsSource: String) {
    databaseDef withSession {
      implicit session =>
        //        ddl.drop
        ddl.create
        val insertResult = (closureComputations returning closureComputations.map(_.id)) +=(None, factsSource)
        closureCompNumber = insertResult
    }
  }

  def closeClosureComputation() {

  }

  def closingTheWorld() {

  }

  def ruleFiring(rule: Rule, bindings: EvalContext, isThenBranch: Boolean, inputFacts: List[Predicate], nestedDependencyId: Int) = {
    val rf = new RuleApplicationData(ruleFiringSerial, rule, bindings, isThenBranch, inputFacts, nestedDependencyId)
    ruleFiringSerial += 1
    rads += rf
    rf
  }

  case class RuleApplication(id: Int, ccId: Int, ruleName: String, thenBranch: Boolean, nested: Int)

  class RuleApplications(tag: Tag) extends Table[RuleApplication](tag, "rule_application") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def ccId = column[Int]("cc_id")

    def ruleName = column[String]("rule")

    def thenBranch = column[Boolean]("thenBranch")

    def nested = column[Int]("nested")

    def * = (id, ccId, ruleName, thenBranch, nested) <>(RuleApplication.tupled, RuleApplication.unapply)
  }

  val ruleApplications = TableQuery[RuleApplications]

  class VarMappings(tag: Tag) extends Table[(Int, String, String)](tag, "variable_mapping") {
    def raId = column[Int]("ra_id")

    def name = column[String]("var")

    def value = column[String]("val")

    def * = (raId, name, value)
  }

  val varMappings = TableQuery[VarMappings]

  // input facts of rule applications
  class InputData(tag: Tag) extends Table[(Int, String, String, String)](tag, "input_data") {
    def raId = column[Int]("ra_id")

    def subj = column[String]("subject")

    def pred = column[String]("predicate")

    def obj = column[String]("object")

    def * = (raId, subj, pred, obj)
  }

  val inputData = TableQuery[InputData]

  // output facts of rule applications
  class OutputData(tag: Tag) extends Table[(Int, String, String, String)](tag, "output_data") {
    def raId = column[Int]("ra_id")

    def subj = column[String]("subject")

    def pred = column[String]("predicate")

    def obj = column[String]("object")

    def * = (raId, subj, pred, obj)
  }

  val outputData = TableQuery[OutputData]

  class Descriptions(tag: Tag) extends Table[(Int, String)](tag, "descriptions") {
    def raId = column[Int]("ra_id")

    def text = column[String]("text")

    def * = (raId, text)
  }

  val descriptions = TableQuery[Descriptions]

  val ddl = ruleApplications.ddl ++ varMappings.ddl ++ closureComputations.ddl ++ inputData.ddl ++ outputData.ddl ++ descriptions.ddl

  // alternative: come up with literal representation at Term's construction time (lazy val?)
  private def factTuples(raId: Int, predicates: List[Predicate]) =
    predicates.map(_.toLiteral).map {
      case (s, p, o) => (raId, s, p, o)
    }


  def pruneRads() {
    val staying = mutable.Set.empty[Int]

    def addNested(id: Int) {
      if (id >= 0) {
        if (staying.add(id)) {
          addNested(rads(id).nestedDependencyId)
        }
      }
    }

    for (i <- rads.size - 1 to 0 by -1) {
      val rad = rads(i)
      if (staying.contains(rad.id)) {
        // nothing else to do
      } else if (rad.outputFacts.nonEmpty) {
        staying.add(rad.id)
        addNested(rad.nestedDependencyId)
      }
    }
    val result = staying.toVector.sorted.map(id => rads(id))
    println(result)
  }

  def storeLog() {
    pruneRads()
    databaseDef withSession {
      implicit session =>
        ddl.create
        rads.foreach {
          rad =>
            val raId = rad.id // the id of the ruleApplication: the foreign key for rule application's data
            ruleApplications += RuleApplication(raId, closureCompNumber, rad.rule.name.toUri, rad.isThenBranch, rad.nestedDependencyId)
            if (rad.isThenBranch) {
              varMappings ++= rad.bindings.tuples.map(pair => (raId, pair._1, pair._2))
              inputData ++= factTuples(raId, rad.inputFacts)
            }
            outputData ++= factTuples(raId, rad.outputFacts.toList)
            descriptions ++= rad.descriptions.map((raId, _))
        }
    }
  }
}
