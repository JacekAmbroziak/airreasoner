package com.trustlayers.reasoner.airj

import com.trustlayers.reasoner.exec.Rule

/**
 * User: jacek
 * Date: 4/27/14
 */

class RuleApplicationEvent(val rule: Rule, val isThenBranch: Boolean) extends Event {

  object Branch extends Enumeration {
    val Then, Else = Value
  }

}
