package com.trustlayers.reasoner

import org.rogach.scallop.ScallopConf

/**
 * A class to parse command line options.
 */
class CLIValidator(args: Seq[String]) extends ScallopConf(args) {
  guessOptionName = true
  version("Trustlayers version 0.1 Alpha")
  banner( """Usage: datastoreDir [load|policy] (inputFile)+
            |Options:
            | """.stripMargin)
  val datastore = opt[String]("datastore", descr = "Data store directory", required = true)
  val fact = opt[String]("factfile", descr = "fact file to load")
  val ont = opt[String]("ontfile", descr = "ontology file to load")
  val policy = opt[String]("policyfile", descr = "policy file to run")
  requireOne(fact, policy)
  mutuallyExclusive(ont, policy)
}
