package com.trustlayers.reasoner.n3

abstract class NamedVariable extends NamedResource

case class UniversalVariable(name: NsName) extends NamedVariable

case class ExistentialVariable(name: NsName) extends NamedVariable
