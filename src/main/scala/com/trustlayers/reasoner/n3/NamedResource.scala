package com.trustlayers.reasoner.n3

import com.trustlayers.reasoner.n3.N3Parser.Node

abstract class NamedResource extends Node {
  val name: NsName
}

case class Resource(name: NsName) extends NamedResource

case class Relation(name: NsName) extends NamedResource

case class MathOp(name: NsName) extends NamedResource

case class MathComp(name: NsName) extends NamedResource