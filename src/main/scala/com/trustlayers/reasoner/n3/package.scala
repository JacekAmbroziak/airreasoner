package com.trustlayers.reasoner

package object n3 {
  val rdf_syntax_ns = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  val rdf_schema_ns = "http://www.w3.org/2000/01/rdf-schema#"
}
