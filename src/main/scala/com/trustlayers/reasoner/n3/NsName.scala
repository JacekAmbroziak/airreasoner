package com.trustlayers.reasoner.n3

import com.trustlayers.reasoner.n3.N3Parser.Node

case class NsName(uri: String, local: String) extends Node {
  override def toString: String = s"NsName($local, $uri)"

  def toUri = uri + local
}
