package com.trustlayers.reasoner.n3

import scala.util.parsing.combinator.JavaTokenParsers
import scala.util.Try
import java.io.Reader
import com.trustlayers.reasoner.air.{AllResources, IsA}

/*
 * N3Parser is an N3 parser which takes in a Reader (via parse()) and produces a set of Nodes which are formed into a
 * List of TopLevels (Statements, Variable, Prefixes and Comments).
 */
object N3Parser extends JavaTokenParsers with AllResources {
  // a way, suggested at StackOverflow to effectively remove comments by treating them as whitespace
  // http://stackoverflow.com/questions/20925434/how-to-ignore-single-line-comments-in-a-parser-combinator
  override protected val whiteSpace = """(\s|#.*)+""".r

  /*
  * classes with names ending with 0
  * represent raw parsing results with prefixed names not yet resolved
  * they are all private to this object
  * Their counterparts without trailing 0 are final, namespace-resolved data structures
  *
  * Package private is found throughout this class in order to facilitate testing.
  * Future maintainers are strongly encouraged to get rid of this if there's a more elegant way
  * to do so. Scalaunit's PrivateMethod trait is an alternative, but is kind of a pain...
  * */

  def parse(reader: Reader): Try[List[TopLevel]] = {
    parseAll(document, reader) match {
      case N3Parser.Success(result, _) => Try(result.map(_.get))
      case N3Parser.Failure(msg, input) => util.Failure(new Exception(input.pos + " " + msg))
      case N3Parser.Error(msg, _) => util.Failure(new Exception(msg))
    }
  }

  case class Comment(text: String) extends TopLevel

  case class PrefixDecl(prefix: String, uri: String) extends TopLevel

  case class ExistentialVariables(list: List[Variable])

  case class Symbol(uri: String) extends Node

  trait Node

  trait TopLevel extends Node

  case class Property(pred: Node, obj: Node) extends Node

  case class Properties(list: List[Property]) extends Node

  case class Statement(subj: Node, pred: Node, obj: Node) extends TopLevel

  case class Variable(name: NsName)

  case class UniversalVariables(list: List[Variable]) extends TopLevel

  case class StringLiteral(value: String) extends Node

  case class IntegerLiteral(value: Int) extends Node

  case class N3List(elements: List[Node]) extends Node

  case class Statements(statements: List[Statement]) extends Node

  case class Formula(list: List[Variable], statements: Statements) extends Node

  private[n3] case class QName(prefix: String, local: String) extends Node {
    def resolve(mapping: Map[String, String]): Try[NsName] =
      mapping.get(prefix) match {
        case Some(uri) => util.Success(NsName(uri, local))
        case None => util.Failure(new Exception(s"unknown prefix '$prefix', local: '$local'"))
      }
  }

  private[n3] case class Variable0(qn: QName) {
    def resolve(mapping: Map[String, String]) = Try(Variable(qn.resolve(mapping).get))
  }

  private[n3] case class UniversalVariables0(list: List[Variable0]) {
    def resolve(mapping: Map[String, String]) = Try(UniversalVariables(list.map(_.resolve(mapping)).map(_.get)))
  }

  private[n3] case class ExistentialVariables0(list: List[Variable0]) {
    def resolve(mapping: Map[String, String]) =
      Try(ExistentialVariables(list.map(_.resolve(mapping)).map(_.get)))
  }

  private[n3] case class Property0(pred: Node, obj: Node) extends Node {
    def resolve(mapping: Map[String, String]): Try[Property] =
      for {
        p <- resolveNs(pred, mapping)
        o <- resolveNs(obj, mapping)
      } yield Property(maybeKnown(p), maybeKnown(o))
  }

  /**
   * This class is introduced as a typed container for lists of raw Property0 parser results
   * It will be resolved to another container type, Properties
   */
  private[n3] case class Properties0(list: List[Property0]) extends Node

  private[n3] case class Statement0(subj: Node, pred: Node, obj: Node) {
    def resolve(mapping: Map[String, String]): Try[Statement] =
      for {
        s <- resolveNs(subj, mapping)
        p <- resolveNs(pred, mapping)
        o <- resolveNs(obj, mapping)
      } yield Statement(s, maybeKnown(p), maybeKnown(o))
  }

  private[n3] object Statement0 extends Node {
    def create(subj: Node, pred: Node, obj: Node): Statement0 =
      pred match {
        case QName("", "a") => Statement0(subj, IsA, obj)
        case _ => Statement0(subj, pred, obj)
      }
  }

  private[n3] case class Formula0(variables: List[Variable0], statements: List[Statement0]) extends Node {
    def resolve(mapping: Map[String, String]): Try[Formula] =
      Try(Formula(variables.map(_.resolve(mapping)).map(_.get),
        Statements(statements.map(_.resolve(mapping)).map(_.get))))
  }

  private[n3] def universalVariables: Parser[UniversalVariables0] =
    "@forAll" ~> repsep(variable, ",") <~ "." ^^ UniversalVariables0

  private[n3] def existentialVariables: Parser[ExistentialVariables0] =
    "@forSome" ~> repsep(variable, ",") <~ "." ^^ ExistentialVariables0

  private[n3] def comment: Parser[Comment] = """(?m)^#.*$""".r ^^ Comment // (?m) for multiline

  private def document: Parser[List[Try[TopLevel]]] = rep(comment | prefixDecl | universalVariables | statement) ^^ {
    resolveListNs(_, Map("" -> "#")) // predefined uri for default namespace as per N3 spec
  }

  private def resolveListNs(topLevel: List[Any], mapping: Map[String, String]): List[Try[TopLevel]] =
    topLevel match {
      case head :: tail => head match {
        case Comment(_) => resolveListNs(tail, mapping) // ignore the comment
        case PrefixDecl(prefix, uri) => resolveListNs(tail, mapping + (prefix -> uri))
        case UniversalVariables0(list) => UniversalVariables0(list).resolve(mapping) :: resolveListNs(tail, mapping)
        case Statement0(subj, pred, obj) => Statement0(subj, pred, obj).resolve(mapping) :: resolveListNs(tail, mapping)
        case elements: List[Any] => resolveListNs(elements, mapping) ::: resolveListNs(tail, mapping)
        case _ => println("???"); resolveListNs(tail, mapping)
      }
      case Nil => Nil
    }

  def resolveNs(element: Any, mapping: Map[String, String]): Try[Node] =
    element match {
      case nn: NsName => util.Success(nn) // already resolved
      case qn: QName => qn.resolve(mapping) // try to resolve prefix -> namespace URI
      case s: Statement0 => s.resolve(mapping)
      case p: Property0 => p.resolve(mapping)
      case pp: Properties0 => Try(Properties(pp.list.map(_.resolve(mapping)).map(_.get)))
      case f: Formula0 => f.resolve(mapping)
      case N3List(elements) => Try(N3List(elements.map(resolveNs(_, mapping)).map(_.get)))
      case literal: StringLiteral => util.Success(literal)
      case literal: IntegerLiteral => util.Success(literal)
      case symbol: Symbol => util.Success(symbol)
      case resource: NamedResource => util.Success(resource)
      case _@x => util.Failure(new Exception("not implemented " + x))
    }

  private[n3] def statement: Parser[List[Statement0]] =
    subj ~ repsep(pred ~ repsep(obj, ","), ";") <~ "." ^^ {
      case ~(subj, list) => list flatMap {
        case ~(pred, objects) => objects map {
          Statement0.create(subj, pred, _)
        }
      }
    }

  private[n3] def properties: Parser[Properties0] = "[" ~> repsep(property, ";") <~ "]" ^^ Properties0

  private[n3] def property: Parser[Property0] =
    pred ~ obj ^^ {
      case ~(pred, obj) => Property0(pred, obj)
    }

  private def n3list: Parser[N3List] =
    "(" ~> rep(strLiteral | intLiteral | qName | n3list | formula) <~ ")" ^^ (N3List(_))

  private def subj: Parser[Node] = qName | formula | properties | n3list | symbol | strLiteral | intLiteral

  private def pred: Parser[Node] = qName | symbol

  private def obj: Parser[Node] = qName | formula | properties | n3list | symbol | strLiteral | intLiteral

  private[n3] def formula: Parser[Formula0] =
    "{" ~> opt(existentialVariables) ~ rep(statement) <~ "}" ^^ {
      case ~(Some(vars), statement :: tail) => Formula0(vars.list, statement ++ tail.flatten)
      case ~(None, statement :: tail) => Formula0(Nil, statement ++ tail.flatten)
      case _ => Formula0(Nil, Nil)
    }

  private[n3] def prefixDecl: Parser[PrefixDecl] = "@prefix" ~> prefix ~ ":" ~ symbol <~ "." ^^ {
    case ~(~(p, ":"), ns) => PrefixDecl(p, ns.uri)
  }

  private[n3] def qName: Parser[QName] = opt(prefix <~ ":") ~ name ^^ {
    case ~(Some(p), local) => QName(p, local)
    case ~(None, local) => QName("", local)
  }

  private[n3] def variable: Parser[Variable0] = qName ^^ Variable0

  private def symbol: Parser[Symbol] = "<" ~> uri <~ ">" ^^ Symbol

  private def prefix: Parser[String] = ident | ""

  private def uri: Parser[String] = """[^>]+""".r

  private def name: Parser[String] = """[a-zA-Z_][a-zA-Z_0-9\-]*""".r

  private def strLiteral: Parser[StringLiteral] = stringLiteral ^^ StringLiteral

  private def intLiteral: Parser[IntegerLiteral] = decimalNumber ^^ {
    case num: String => IntegerLiteral(num.toInt)
  }
}