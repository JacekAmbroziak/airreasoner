package com.trustlayers.reasoner

import com.hp.hpl.jena.rdf.model._
import com.hp.hpl.jena.reasoner._

import com.hp.hpl.jena.reasoner.rulesys._
import scala.collection.JavaConverters._
import java.io.{BufferedReader, StringReader}

/**
 * A draft interface for interacting with the underlying reasoning logic
 */
class Reasoner(rules: String, ontology: Option[Model], facts: Model) {
  val input = new BufferedReader(new StringReader(rules))

  // TODO: Reasoner configuration
  val reasoner = new GenericRuleReasoner(Rule.parseRules(Rule.rulesParserFromReader(input)))
  val infModel = ontology match {
    case Some(ont) => ModelFactory.createInfModel(reasoner, ont, facts)
    case None => ModelFactory.createInfModel(reasoner, facts)
  }
  infModel.setDerivationLogging(true)

  def getDerivation(statement: Statement): Iterator[Derivation] = infModel.getDerivation(statement).asScala

  // Returns all AIR 'compliant' assertions
  def getCompliance = getPropertyMatches(infModel.createProperty(air_ns, "compliant-with"))

  // Returns all AIR 'non-compliant' assertions
  def getNonCompliance = getPropertyMatches(infModel.createProperty(air_ns, "non-compliant-with"))

  private def getPropertyMatches(property: Property) = infModel.listStatements(null, property, null).asScala
}
