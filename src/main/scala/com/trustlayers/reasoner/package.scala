package com.trustlayers

/**
 * User: jacek
 * Date: 4/21/14
 */

package object reasoner {
  val air_ns: String = "http://dig.csail.mit.edu/TAMI/2007/amord/air#"

  def concat(lines: Seq[String]) = lines.foldLeft(new StringBuilder)(_.append(_).append("\n")).toString()
}
