package com.trustlayers.reasoner

import com.trustlayers.reasoner.datastore.Datastore

object ReasonerMain {
  def main(args: Array[String]) {
    val opts = new CLIValidator(args)
    val datastore = new Datastore(Some(opts.datastore()))
    if (opts.fact.isDefined) {
      datastore.loadFacts(List(opts.fact()))
      if (opts.ont.isDefined) datastore.loadOntologies(List(opts.ont()))
    }
    else {
      assert(opts.policy.isDefined)
      val reasoner = new Reasoner(opts.policy(), Some(datastore.getOntologyModelForReasoning), datastore.getFactsModelForReasoning)
      val positiveAssertions = reasoner.getCompliance
      val negativeAssertions = reasoner.getNonCompliance
      positiveAssertions.foreach(assertion => {
        println(assertion + " comes from: ")
        reasoner.getDerivation(assertion).foreach(derivation => println(derivation.toString))
      })
      negativeAssertions.foreach(assertion => {
        println(assertion + " comes from: ")
        reasoner.getDerivation(assertion).foreach(derivation => println(derivation.toString))
      })
    }
  }
}
