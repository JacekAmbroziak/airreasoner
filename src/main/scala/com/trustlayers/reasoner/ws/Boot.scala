package com.trustlayers.reasoner.ws

import akka.actor.ActorSystem
import spray.routing.SimpleRoutingApp
import spray.http.MediaTypes._
import com.trustlayers.reasoner.exec.{AirProgram, ReasoningEngine}
import com.trustlayers.reasoner.airj.JustificationLogger
import com.trustlayers.reasoner.exec.dataSourceFromString
import com.trustlayers.reasoner.exec.dataSourceFromURL
import com.trustlayers.reasoner.air._
import java.net.{URL, MalformedURLException}
import java.io.{StringWriter, PrintWriter}

object Boot extends App with SimpleRoutingApp {

  def isValidURL(candidate: String) = {
    var result = false
    try {
      new URL(candidate)
      result = true
    } catch {
      case mue: MalformedURLException => ;
    }
    result
  }

  implicit val system = ActorSystem("trustlayers")

  // TODO: Make these configurable based on command line
  startServer(interface = "localhost", port = 8080) {

    // TODO: Endpoints for adding data and for reasoning about the data already in the system.
    val index = """<!DOCTYPE html><html><body>
<form name="input" action="stateless-reasoner" method="GET">
<br>Rule (rule definition):<br>
<textarea rows="10" cols="30" name="rules">
</textarea>

<br>Facts (facts definition):<br>
<textarea rows="10" cols="30" name="facts">
</textarea>

<br><input type="submit" value="Submit">
</form>

</body>
</html>
"""
    get {
      pathSingleSlash {
        respondWithMediaType(`text/html`) {
          complete(index)
        }
      } ~
      path("stateless-reasoner") {
        parameter("facts","rules") {
          (facts: String, rules: String) => {
            var result = "foobar"
            try {
              implicit val jlog = new JustificationLogger
              val factsSource = if (isValidURL(facts)) dataSourceFromURL(facts) else dataSourceFromString(facts, "n3")
              val rulesSource = if (isValidURL(rules)) dataSourceFromURL(rules) else dataSourceFromString(rules, "n3")

              val engine = new ReasoningEngine(factsSource, AirProgram.parse(rulesSource).get)

              engine.init()
              engine.runLoop()

              val model = engine.model
              val airCompliantWith = model.createProperty(air_ns, "compliant-with")
              val airNonCompliantWith = model.createProperty(air_ns, "non-compliant-with")

              engine.listStatements(model, airCompliantWith)
              engine.listStatements(model, airNonCompliantWith)

              jlog.storeLog()
            } catch {
              case e: Exception => result = {
                val writer = new StringWriter()
                e.printStackTrace(new PrintWriter(writer))
                writer.toString
              }
            }
            complete(result)
          }
        }
      } ~
        path("foo") {
          complete("Foo")
        }
    }
  }
}