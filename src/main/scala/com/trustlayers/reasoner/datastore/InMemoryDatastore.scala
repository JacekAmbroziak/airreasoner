package com.trustlayers.reasoner.datastore

import java.io.{InputStream, ByteArrayInputStream}

/**
 * A class for test purposes which uses an in-memory variant of TDB
 */
class InMemoryDatastore extends Datastore(None) {
  override def makeInputStream(input: String): InputStream = new ByteArrayInputStream(input.getBytes)
}
