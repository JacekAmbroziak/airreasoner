package com.trustlayers.reasoner.datastore

import com.hp.hpl.jena.query.{QueryFactory, ReadWrite}
import org.w3.banana._
import org.w3.banana.jena._
import com.hp.hpl.jena.tdb.TDBFactory
import java.io.{InputStream, FileInputStream}
import org.w3.banana.jena.{Jena, JenaStore}
import JenaOperations._
import scala.collection.JavaConverters._
import com.hp.hpl.jena.graph.NodeFactory
import com.hp.hpl.jena.graph.{Triple => JenaTriple, Node => JenaNode}

/**
 * A class to load data into the underlying storage.
 * TODO: Clean up API on this class, it's a bit of a mess
 */
class Datastore(destinationDirectory: Option[String]) {
  val ontologyURI = "http://trustlayers.com/ontology-graph"
  val factsURI = "http://trustlayers.com/base-graph"

  // TODO: refactor this so none of the Test variant of this class bleeds through
  // Note that emptyString is not good enough here, we need to call the no-arg variant
  val ds = destinationDirectory match {
    case Some(dir) => TDBFactory.createDataset(dir)
    case None => TDBFactory.createDataset()
  }

  // Unclear if we actually need a defensive copy at this point, better safe than sorry.
  val datastore = JenaStore(ds, defensiveCopy = true)

  def makeInputStream(input: String): InputStream = new FileInputStream(input)

  // Unfortunately Jena has a mess of different interfaces...
  def getFactsModelForReasoning = {
    ds.begin(ReadWrite.READ)
    try {
      ds.getNamedModel(factsURI)
    } finally ds.end()
  }

  def getOntologyModelForReasoning = {
    ds.begin(ReadWrite.READ)
    try {
      ds.getNamedModel(ontologyURI)
    } finally ds.end()
  }

  // TODO: Maybe this should be private.
  // If so, we should drop delete() as well.
  def load(input: String, context: String) = {
    // TODO: Can this fail without throwing? Yes. Fix it!
    // TODO: User feedback for each file loaded...
    try {
      datastore.execute {
        Command.append[Jena](
          makeUri(context),
          graphToIterable(RDFReader[Jena, Turtle].read(makeInputStream(input), "").get))
      }.getOrFail()
    } catch {
      case e: Exception => println("Unable to load file: " + e.getMessage)
    }
  }

  def loadFacts(facts: List[String]) {
    facts.foreach(load(_, factsURI))
  }

  def loadOntologies(ontologies: List[String]) {
    ontologies.foreach(load(_, ontologyURI))
  }

  /*
   * context should be a unique value, which will be used to filter out
   */
  def assertNewTriple(subj: String, pred: String, obj: String, context: String) = {
    // TODO: Use JenaStore::appendToGraph instead?
    val s = NodeFactory.createLiteral(subj)
    val p = NodeFactory.createURI(pred)
    val o = NodeFactory.createLiteral(obj)
    val list = List(new JenaTriple(s, p, o))
    // TODO: Can we set this up in a way which does an in-memory commit instead of writing back to the datastore?
    // This should be possible if we use sparql, in theory. See: http://jena.apache.org/documentation/tdb/assembler.html#graph
    datastore.execute {
      Command.append[Jena](makeUri(context), list)
    }
  }

  /*
     We currently expect querystring to draw data out of two graphs: the TLdefault graph and the graph
     associated with this policy invocation.
   */
  def SPARQLQuery(querystring: String) = {
    val query = QueryFactory.create(querystring)
    // TODO: Make this work more cleanly with Scala futures, rather than just blocking
    SparqlEngine[Jena](datastore).executeSelect(query).getOrFail().asScala
  }

  def delete(context: String) {
    datastore.execute {
      Command.delete[Jena](makeUri(context))
    }
  }
}
