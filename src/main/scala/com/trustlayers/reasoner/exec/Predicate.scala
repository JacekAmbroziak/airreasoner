package com.trustlayers.reasoner.exec

abstract class Predicate {
  def toString(ctx: EvalContext): String

  def instantiate(ctx: EvalContext): Predicate

  def toLiteral: (String, String, String)
}
