package com.trustlayers.reasoner.exec

import scala.collection.mutable.ListBuffer
import scala.collection.mutable
import scala.util.{Failure, Try}
import com.trustlayers.reasoner.n3.NsName
import com.trustlayers.reasoner.air.AirRulesProcessor

class AirProgram(val uri: String) {
  val policies = ListBuffer.empty[Policy]
  val ruleSets = ListBuffer.empty[RuleSet]
  val ruleBuffer = ListBuffer.empty[Rule]
  val ruleMap = mutable.Map.empty[NsName, Rule]
  var jenaRuleSerialNumber = 0
  var airRuleSerialNumber = 0

  def addRuleSet(ruleSet: RuleSet) {
    ruleSets += ruleSet
  }

  def addPolicy(policy: Policy) {
    policies += policy
  }

  def addRule(rule: Rule) {
    ruleBuffer += rule
    ruleMap += (rule.name -> rule)
  }

  def nextJenaRuleNumber = {
    jenaRuleSerialNumber += 1
    jenaRuleSerialNumber
  }

  def nextAirRuleNumber = {
    airRuleSerialNumber += 1
    airRuleSerialNumber
  }

  def nextJenaRuleName = "r" + nextJenaRuleNumber

  def nextAirRuleName = NsName("#", "g" + nextAirRuleNumber)

  def ruleList = ruleBuffer.toList

  def ruleMapJena = ruleList.map(r => (r.jenaName, r)).toMap

  def topRules = ruleSets.toList.flatMap(_.ruleNames).toSet.toList.map(ruleMap.get).flatten.toSeq

  def findRule(name: NsName) = ruleMap.get(name)
}

object AirProgram {
  def parse(source: DataSource): Try[AirProgram] =
    AirRulesProcessor.extractAirResources(source.getReader)
      .transform(resources => RuleCompiler.translate(resources, source.uri), f => Failure(f))
}
