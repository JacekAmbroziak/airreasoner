package com.trustlayers.reasoner.exec

import com.hp.hpl.jena.rdf.model._
import scala.util.{Failure, Success, Try}
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype
import com.trustlayers.reasoner.n3.NamedVariable

abstract class TermNode {
  def toString(ctx: EvalContext): String

  def toLiteral: String

  def toRDFNode(model: Model): Try[RDFNode]

  def toResource(model: Model): Try[Resource]

  def toProperty(model: Model): Try[Property]
}

case class TermNodeList(nodes: List[TermNode]) extends TermNode {
  def toString(ctx: EvalContext): String = nodes.map(_.toString(ctx)).mkString("(", ",", ")")

  def toLiteral: String = toString

  override def toString = nodes.mkString("(", ",", ")")

  def toRDFNode(model: Model) = Failure(new Exception("NodeList to Resource: " + nodes))

  def toResource(model: Model) = Failure(new Exception("NodeList to Resource: " + nodes))

  def toProperty(model: Model) = Failure(new Exception("NodeList to Property: " + nodes))
}

case class TermVariable(jenaName: String, airVar: NamedVariable) extends TermNode {
  def toString(ctx: EvalContext) = ctx.map.get(this).fold(jenaName)(_.toString)

  def toLiteral: String = throw new Exception("variable toLiteral? " + uri)

  def uri = airVar.name.toUri

  override def toString = airVar.name.toString

  override def hashCode(): Int = airVar.hashCode()

  override def equals(other: scala.Any): Boolean =
    other match {
      case tv: TermVariable => tv.airVar eq airVar
      case _ => false
    }

  // variables in assert and description actions should already be substituted with values from bindings
  // hence attempting in later stages to turn them to nodes should fail

  def toRDFNode(model: Model) = Failure(new Exception(jenaName + " being mapped to RDFNode"))

  def toResource(model: Model) = Failure(new Exception(jenaName + " being mapped to Resource"))

  def toProperty(model: Model) = Failure(new Exception(jenaName + " being mapped to Property"))
}

case class UriRef(uri: String) extends TermNode {
  def toString(ctx: EvalContext): String = uri

  def toLiteral: String = "<" + uri + ">"

  override def toString = uri

  def toRDFNode(model: Model) = Success(model.getResource(uri))

  def toResource(model: Model) = Success(model.getResource(uri))

  def toProperty(model: Model) = Success(model.getProperty(uri))
}

case class BlankNode(anonId: AnonId) extends TermNode {
  def toString(ctx: EvalContext): String = toString

  def toLiteral: String = toString

  override def toString = "<#_" + anonId.toString + ">"

  def toRDFNode(model: Model) = Success(model.createResource(anonId))

  def toResource(model: Model) = Success(model.createResource(anonId))

  def toProperty(model: Model) = Failure(new Exception("mapping a blank node to Property: " + anonId))
}

case class StringLiteral(text: String) extends TermNode {
  def toString(ctx: EvalContext): String = text

  def toLiteral: String = "\"" + text + "\""

  override def toString = text

  def toRDFNode(model: Model) = Success(model.createTypedLiteral(text, XSDDatatype.XSDstring))

  def toResource(model: Model) = Failure(new Exception("StringLiteral to Resource: " + text))

  def toProperty(model: Model) = Failure(new Exception("StringLiteral to Property: " + text))
}

case class IntegerLiteral(n: Int) extends TermNode {
  def toString(ctx: EvalContext): String = toString

  def toLiteral: String = "\"" + n + "\"^^xsd:int"

  override def toString = n.toString

  def toRDFNode(model: Model) = Success(model.createTypedLiteral(new Integer(n), XSDDatatype.XSDint))

  def toResource(model: Model) = Failure(new Exception("IntegerLiteral to Resource: " + n))

  def toProperty(model: Model) = Failure(new Exception("IntegerLiteral to Property: " + n))
}

