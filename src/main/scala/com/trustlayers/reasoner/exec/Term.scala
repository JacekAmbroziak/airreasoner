package com.trustlayers.reasoner.exec

import com.hp.hpl.jena.rdf.model.{Statement, Model}
import scala.util.Try

case class Term(subj: TermNode, pred: TermNode, obj: TermNode) extends Predicate {

  def toString(ctx: EvalContext) = "(%s %s %s)".format(subj.toString(ctx), pred.toString(ctx), obj.toString(ctx))

  override def toString = s"($subj $pred $obj)"

  def instantiate(ctx: EvalContext): Term = new Term(ctx.mapVar(subj), ctx.mapVar(pred), ctx.mapVar(obj))

  def toStatement(model: Model): Try[Statement] =
    for {
      s <- subj.toResource(model)
      p <- pred.toProperty(model)
      o <- obj.toRDFNode(model)
    } yield model.createStatement(s, p, o)

  // translation into turtle/n3 triple for justification logging purposes
  def toLiteral: (String, String, String) = (subj.toLiteral, pred.toLiteral, obj.toLiteral)
}