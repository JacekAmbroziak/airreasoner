package com.trustlayers.reasoner.exec

/**
 * User: jacek
 * Date: 5/8/14

 Corresponds to empty formula { } which is always true and that can be used
 as a trivially true air:if condition used to simply execute air:then
 unconditionally

 */

object True extends Predicate {
  override def toString: String = "true()"

  def toString(ctx: EvalContext): String = toString

  def instantiate(ctx: EvalContext): Predicate = this

  def toLiteral: (String, String, String) = ("#dummy", "", "")
}
