package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.n3.NsName

abstract class HeadAction {
  def instantiate(bindings: EvalContext): HeadAction

  def perform(engine: ReasoningEngine, bindings: EvalContext, rad: RuleApplicationData)
}

class AssertAction(val terms: List[Term]) extends HeadAction {
  override def toString = "assert:" + terms.mkString

  override def instantiate(ctx: EvalContext) = new AssertAction(terms.map(_.instantiate(ctx)))

  override def perform(engine: ReasoningEngine, bindings: EvalContext, rad: RuleApplicationData) {
    engine.assert(terms.map(_.instantiate(bindings)), rad)
  }

  override def equals(obj: scala.Any): Boolean =
    obj match {
      case that: AssertAction => terms.equals(that.terms)
      case _ => false
    }
}

class ActivateNestedRuleAction(val rule: Rule, val bindings: EvalContext = EvalContext.empty) extends HeadAction {
  override def instantiate(bindings: EvalContext) = new ActivateNestedRuleAction(rule, bindings)

  override def perform(engine: ReasoningEngine, bindings0: EvalContext, rad: RuleApplicationData) {
    // bindings and bindings0 are the same
    engine.activateRule(rule, bindings, rad)
  }

  override def toString = "activate:" + rule
}

class ActivateNamedRuleAction(val ruleName: NsName, val bindings: EvalContext = EvalContext.empty) extends HeadAction {
  override def instantiate(bindings: EvalContext) = new ActivateNamedRuleAction(ruleName, bindings)

  override def perform(engine: ReasoningEngine, bindings0: EvalContext, rad: RuleApplicationData) {
    engine.activateRule(ruleName, bindings, rad)
  }

  override def toString = "activate:" + ruleName
}

class DescriptionAction(val ruleName: NsName, val terms: List[TermNode]) extends HeadAction {
  override def instantiate(ctx: EvalContext) = new DescriptionAction(ruleName, terms.map(ctx.mapVar))

  override def perform(engine: ReasoningEngine, bindings: EvalContext, rad: RuleApplicationData) {
    engine.description(ruleName, terms.mkString(" "), rad)
  }

  override def equals(obj: scala.Any): Boolean =
    obj match {
      case that: DescriptionAction => ruleName.equals(that.ruleName) && terms.equals(that.terms)
      case _ => false
    }
}
