package com.trustlayers.reasoner.exec

import scala.collection.mutable

trait RuleActivationSource

class RuleActivationData(val id: Int, val rule: Rule, val bindings: EvalContext, val source: RuleActivationSource) {
  var nestedRuleActivation: Option[Rule] = None

  def activate(nestedRule: Rule) {
    nestedRuleActivation = Some(nestedRule)
  }

  override def toString: String = id + " " + " " + rule.name.toUri
}

class RuleApplicationData(val id: Int,
                          val rule: Rule, val bindings: EvalContext,
                          val isThenBranch: Boolean,
                          val inputFacts: List[Predicate],
                          val nestedDependencyId: Int) {

  var outputFacts = mutable.ListBuffer.empty[Term]
  val descriptions = mutable.ListBuffer.empty[String]
  val nestedRuleActivations = mutable.ListBuffer.empty[RuleActivation]

  def assert(terms: List[Term]) {
    outputFacts ++= terms
  }

  def assert(term: Term) {
    outputFacts += term
  }

  def addDescription(text: String) {
    descriptions += text
  }

  def activate(ra: RuleActivation) {
    nestedRuleActivations += ra
  }

  override def toString: String = id + " " + isThenBranch + " " + rule.name.toUri
}
