package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.n3.NsName

/*
seq of variables is shared between body and head
the order matters for extraction of Jena bindings
 */

class Rule(val name: NsName,
           val label: String,
           val jenaName: String,
           val vars: List[TermVariable],
           val body: RuleBody,
           val thenHead: RuleHead,
           val elseHead: RuleHead) {

  override def toString = s"$name [$jenaName: $body -> $builtinName()]\nhead: $thenHead"

  def toJenaString(ctx: EvalContext) = s"[$jenaName: ${body.toString(ctx)} -> $builtinName(${vars.map(_.jenaName).mkString(",")})]"

  def instantiateThenActions(bindings: EvalContext) = thenHead.instantiateActions(bindings)

  def instantiateElseActions(bindings: EvalContext) = elseHead.instantiateActions(bindings)

  def instantiate(newJenaName: String, bindings: EvalContext) = {
    // retain variables that are mapped to nodes
    val map = bindings.map.filterKeys(vars.contains)
    val substitution = EvalContext(map)
    // the instantiated rule will have fewer free variables
    new Rule(name, label, newJenaName,
      vars.filterNot(map.contains), // retain only not yet bound variables, ie. free variables
      body.instantiate(substitution),
      thenHead.instantiate(substitution),
      elseHead.instantiate(substitution))
  }

  def inputFacts(bindings: EvalContext) = body.predicates.map(_.instantiate(bindings))

  def hasElse = elseHead.isNonEmpty

  override def equals(value: scala.Any) = {
    // jenaName is intentionally omitted.
    value match {
      case rhs: Rule =>
      (name.equals(rhs.name)
        && label.equals(rhs.label)
        && vars.equals(rhs.vars)
        && body.equals(rhs.body)
        && thenHead.equals(rhs.thenHead)
        && elseHead.equals(rhs.elseHead)
      )
    }
  }
}

case class RuleBody(predicates: List[Predicate]) {
  def toString(ctx: EvalContext) = predicates.map(_.toString(ctx)).mkString

  def instantiate(bindings: EvalContext) =
    if (bindings.isEmpty) this else new RuleBody(predicates.map(_.instantiate(bindings)))

  override def toString = predicates.mkString
}

case class RuleHead(actions: List[HeadAction]) {
  override def toString = actions.mkString

  def instantiate(bindings: EvalContext) =
    if (bindings.isEmpty) this else new RuleHead(actions.map(_.instantiate(bindings)))

  def instantiateActions(bindings: EvalContext) =
    if (bindings.isEmpty) actions else actions.map(_.instantiate(bindings))

  def isNonEmpty = actions.size > 0
}

case class RuleSet(name: NsName, label: String, ruleNames: List[NsName])
