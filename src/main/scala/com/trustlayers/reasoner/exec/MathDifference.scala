package com.trustlayers.reasoner.exec

class MathDifference(operands: TermNodeList, result: TermNode) extends ArithmeticOperation("difference", operands, result) {

  def instantiate(ctx: EvalContext) = new MathDifference(ctx.mapNodeList(operands.nodes), ctx.mapVar(result))

  override def toString: String = s"-($operands, $result)"
}
