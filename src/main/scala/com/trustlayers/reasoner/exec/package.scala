package com.trustlayers.reasoner

import java.io._
import java.nio.charset.StandardCharsets
import java.net.URL

package object exec {
  val builtinName = "bi"

  def concat(lines: Seq[String]) = lines.foldLeft(new StringBuilder)(_.append(_).append("\n")).toString()

  def buildReader(lines: Seq[String]) = new BufferedReader(new StringReader(concat(lines)))

  def buildReader(line: String) = new BufferedReader(new StringReader(line))

  def resourceReader(name: String) = new InputStreamReader(ClassLoader.getSystemResourceAsStream(name))

  def resourceReader(source: DataSource) = new InputStreamReader(source.in)

  def extension(fileName: String) = fileName.substring(fileName.lastIndexOf('.') + 1).toUpperCase

  def dataSourceFromResource(resourceName: String) =
    DataSource(ClassLoader.getSystemResourceAsStream(resourceName), "", extension(resourceName), resourceName)

  def dataSourceFromFile(fileName: String) = {
    val file = new File(fileName)
    DataSource(new FileInputStream(file), "", extension(fileName), file.toURI.toString)
  }

  // TODO: Figure out the right mapping for the base and URI arguments of DataSource()
  def dataSourceFromURL(url: String) = {
    DataSource(new URL(url).openStream(), "", extension(url), "")
  }

  // TODO: Figure out the right mapping for the base and URI arguments of DataSource()
  def dataSourceFromString(input: String, lang: String) = {
    val stream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8))
    DataSource(stream, "", lang, "")
  }
}
