package com.trustlayers.reasoner.exec

import com.hp.hpl.jena.graph.{Node_Literal, Node_Blank, Node_URI, Node => JenaNode}

/*
 * EvalContext contains the bindings between variables and their values for a given rule instance.
 * TODO: implement more Jena node types incl. literals of various types (or wrap!)
 * https://jena.apache.org/documentation/notes/typed-literals.html
 */

class EvalContext(val map: Map[TermVariable, JenaNode]) {

  def mapVar(node: TermNode): TermNode =
    node match {
      case variable: TermVariable => map.get(variable) match {
        case None => variable
        case Some(node: Node_URI) => UriRef(node.toString)
        case Some(node: Node_Blank) => BlankNode(node.getBlankNodeId)
        case Some(node: Node_Literal) => node.getLiteralValue match {
          case n: Integer => IntegerLiteral(n)
          case s: String => StringLiteral(s)
          case _ => throw new Exception("literal not mapped: " + node)
        }
        case Some(node: Any) => throw new Exception("NYI: not mapped: " + variable + " " + node)
      }
      case _ => node
    }

  def mapNodeList(nodes: List[TermNode]) = TermNodeList(nodes.map(mapVar))

  override def toString = "ExCtx:" + map

  def tuples = map.map {
    case (v, n) => (v.uri, n.toString)
  }

  def isEmpty = map.isEmpty
}

object EvalContext {

  object EmptyContext extends EvalContext(Map.empty)

  def apply(map: Map[TermVariable, JenaNode]): EvalContext = if (map.isEmpty) EmptyContext else new EvalContext(map)

  def apply(vars: List[TermVariable], values: IndexedSeq[JenaNode]): EvalContext = apply(vars.zip(values).toMap)

  def apply(): EvalContext = EmptyContext

  val empty = EmptyContext
}
