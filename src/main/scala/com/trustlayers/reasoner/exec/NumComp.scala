package com.trustlayers.reasoner.exec

sealed trait NumComp {
  def name: String // Jena's builtin equivalent
}

case object GT extends NumComp {
  val name = "greaterThan"
}

case object LT extends NumComp {
  val name = "lessThan"
}

case object GE extends NumComp {
  val name = "ge"
}

case object LE extends NumComp {
  val name = "le"
}

case object EQ extends NumComp {
  val name = "equal"
}

case object NE extends NumComp {
  val name = "notEqual"
}