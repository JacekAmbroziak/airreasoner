package com.trustlayers.reasoner.exec

abstract class ArithmeticOperation(opName: String, operands: TermNodeList, result: TermNode) extends Predicate {
  // TODO currently the length of operands is not verified nor results are produced for lists shorter and longer than 2
  def toString(ctx: EvalContext) =
    "%s(%s,%s,%s)".format(opName, operands.nodes(0).toString(ctx), operands.nodes(1).toString(ctx), result.toString(ctx))

  def toLiteral: (String, String, String) = (operands.toString, opName, result.toString)
}
