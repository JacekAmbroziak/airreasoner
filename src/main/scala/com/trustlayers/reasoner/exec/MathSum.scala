package com.trustlayers.reasoner.exec

class MathSum(operands: TermNodeList, result: TermNode) extends ArithmeticOperation("sum", operands, result) {
  def instantiate(ctx: EvalContext) = new MathSum(ctx.mapNodeList(operands.nodes), ctx.mapVar(result))

  override def toString: String = s"+($operands, $result)"
}
