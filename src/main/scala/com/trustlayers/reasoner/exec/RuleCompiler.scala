package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.n3._
import com.trustlayers.reasoner.n3.N3Parser.{Success => _, Failure => _, _}
import scala.collection.mutable
import scala.util.{Success, Failure, Try}
import com.trustlayers.reasoner.n3.N3Parser.Property
import com.trustlayers.reasoner.n3.NsName
import com.trustlayers.reasoner.n3.N3Parser.Formula
import com.trustlayers.reasoner.n3.N3Parser.N3List
import com.trustlayers.reasoner.n3.N3Parser.Statement
import com.trustlayers.reasoner.n3.N3Parser.Statements
import com.trustlayers.reasoner.n3.N3Parser.Properties
import com.trustlayers.reasoner.n3.UniversalVariable
import com.trustlayers.reasoner.air._

/**
 * RuleCompiler is responsible for producing an AirProgram from it's inputs.
 * The AirProgram is the representation of AIR rules that can be executed.
 *
 * The class is used via a translate() call in its companion object
 * The methods of the class are all private. In fact, the class itself
 * is private, but its companion object is visible.
 */

private class RuleCompiler(val airProgram: AirProgram) {

  private def translate(resources: List[(NsName, NamedResource)]): Try[AirProgram] = {
    Try(resources.map {
      case (name, rule: AirRule) => translate(rule).map(r => airProgram.addRule(r))
      case (name, ruleSet: AirRuleSet) => Success(addRuleSet(ruleSet))
      case (name, uVar: UniversalVariable) => Success(()) // ignore
      case (name, obj) => Failure(new Exception(s"unknown case $name -> $obj"))
    }.map(_.get))
      .transform(s => Success(airProgram), f => Failure(f))
  }

  private def addRuleSet(ruleSet: AirRuleSet) {
    airProgram.addRuleSet(new RuleSet(ruleSet.name, ruleSet.label, ruleSet.ruleNames.toList))
  }

  /*
    AIR has the concept of nested rules: rules that can be activated by Then or Else actions
    If a rule is reusable it makes sense for it to be a top-level, named element of the program
    It can be referred to as part of a Policy/RuleSet, or activated from another rule.
    But rules that are not reusable and are specific to their activation context can be,
    for brevity of code, inlined directly. This actually aids in program readability.
    For generating justifications we need some names for directly nested rules
    so we generate unique names for such rules here
   */
  private def translate(rule: AirRule): Try[Rule] = {
    // name anonymous rules
    val name = if (rule.name != emptyNsName) rule.name else airProgram.nextAirRuleName
    // we need to preserve variables' order of appearance hence LinkedHashMap
    val variables = mutable.LinkedHashMap.empty[NamedResource, TermVariable]

    def toTermVariable(variable: NamedVariable): TermVariable =
      variables.getOrElse(variable, {
        val tv = new TermVariable("?v" + variables.size, variable)
        variables.update(variable, tv)
        tv
      })

    def toTermNode(node: Node): Try[TermNode] =
      node match {
        case NsName(uri, local) => Success(new UriRef(uri + local))
        case N3Parser.StringLiteral(text) => Success(StringLiteral(text))
        case N3Parser.IntegerLiteral(n) => Success(IntegerLiteral(n))
        case N3List(elements) => toNodeList(elements)
        case v: NamedVariable => Success(toTermVariable(v))
        case r: NamedResource => toTermNode(r.name)
        case _ => Failure(new Exception("NYI termNode " + node))
      }

    def toTermNodes(nodes: List[Node]): Try[List[TermNode]] = Try(nodes.map(toTermNode).map(_.get))

    def toNodeList(elements: List[Node]) = toTermNodes(elements).map(termNodes => TermNodeList(termNodes))

    // uses Try/flatMap/map pattern to deal with possible toTermNode's failure for any of s,p,o
    // could also be written using 'for' comprehension
    def toTerm(s: Statement): Try[Term] =
      toTermNode(s.subj).flatMap(subj =>
        toTermNode(s.pred).flatMap(pred =>
          toTermNode(s.obj).map(obj =>
            new Term(subj, pred, obj))))

    def createAssertAction(obj: Node): Try[HeadAction] =
      obj match {
        case Formula(_, Statements(statements)) => Try(new AssertAction(statements.map(toTerm).map(_.get)))
        case _ => Failure(new Exception("Assert, NYI " + obj))
      }

    def createActivateRuleAction(obj: Node): Try[HeadAction] =
      obj match {
        case name: NsName => Success(new ActivateNamedRuleAction(name))
        case _ => Failure(new Exception("ActivateRule, NYI " + obj))
      }

    def createDescriptionAction(ruleName: NsName, obj: Node): Try[HeadAction] =
      obj match {
        case N3List(elements) => toTermNodes(elements).map(terms => new DescriptionAction(ruleName, terms))
        case _ => Failure(new Exception("NYI " + obj))
      }

    def bodyPredicates(node: Node): List[Try[Predicate]] =
      node match {
        case Formula(_, Statements(Nil)) => List(Success(True))
        case Formula(varList, Statements(statements)) => statements.map {
          case Statement(N3List(elements), op: MathOp, o) =>
            for {
              args <- toNodeList(elements)
              result <- toTermNode(o)
            } yield op match {
              case MathSum => new MathSum(args, result)
              case MathDifference => new MathDifference(args, result)
            }
          case Statement(s, comparison: MathComp, o) =>
            for {
              op1 <- toTermNode(s)
              op2 <- toTermNode(o)
            } yield comparison match {
              case MathGreaterThan => new ArithmeticComparison(GT, op1, op2)
              case MathLessThan => new ArithmeticComparison(LT, op1, op2)
              case MathEqualTo => new ArithmeticComparison(EQ, op1, op2)
              case MathNotEqualTo => new ArithmeticComparison(NE, op1, op2)
            }
          case _@statement => toTerm(statement)
        }
        case _ => List(Failure(new Exception("NYI ifClause " + node)))
      }

    def processClause(node: Node): List[Try[HeadAction]] =
      node match {
        case EmptyNode => List.empty[Try[HeadAction]]
        case Properties(list) => list.map {
          case Property(AirAssert, obj) => createAssertAction(obj)
          case Property(AirRuleRelation, obj) => createActivateRuleAction(obj)
          case Property(AirDescription, obj) => createDescriptionAction(name, obj)
          case Property(pred, obj) => Failure(new Exception("NYI clause property " + node))
        }
        case rule: AirRule => List(Try(new ActivateNestedRuleAction(translate(rule).get)))
        case _ => Failure(new Exception("NYI clause " + node)) :: Nil
      }

    def transformToHeadActions(nodes: Seq[Node]): Try[Seq[HeadAction]] =
      Try(nodes.flatMap(processClause).map(_.get))

    for {
      predicates <- Try(bodyPredicates(rule.ifClause).map(_.get))
      headActions <- transformToHeadActions(rule.thenClauses)
      elseActions <- transformToHeadActions(rule.elseClauses)
    } yield new Rule(name,
      rule.label,
      airProgram.nextJenaRuleName,
      variables.values.toList,
      new RuleBody(predicates),
      new RuleHead(headActions.toList),
      new RuleHead(elseActions.toList))
  }
}

object RuleCompiler {
  def translate(resources: Map[NsName, NamedResource], uri: String): Try[AirProgram] =
    new RuleCompiler(new AirProgram(uri)).translate(resources.toList)
}