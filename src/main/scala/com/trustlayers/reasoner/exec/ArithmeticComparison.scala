package com.trustlayers.reasoner.exec

class ArithmeticComparison(op: NumComp, lft: TermNode, rgt: TermNode) extends Predicate {
  override def toString: String = s"${op.name}($lft, $rgt)"

  def toString(ctx: EvalContext) = "%s(%s,%s)".format(op.name, lft.toString(ctx), rgt.toString(ctx))

  def instantiate(ctx: EvalContext) = new ArithmeticComparison(op, ctx.mapVar(lft), ctx.mapVar(rgt))

  def toLiteral: (String, String, String) = (lft.toLiteral, op.name, rgt.toLiteral)
}
