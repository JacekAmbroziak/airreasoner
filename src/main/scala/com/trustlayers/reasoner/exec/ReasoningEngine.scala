package com.trustlayers.reasoner.exec

import com.hp.hpl.jena.reasoner.rulesys.builtins.BaseBuiltin
import com.hp.hpl.jena.graph.{Node => JenaNode}
import com.hp.hpl.jena.reasoner.rulesys.{Rule => JenaRule, BuiltinRegistry, GenericRuleReasoner, RuleContext}
import scala.collection.JavaConversions._
import scala.collection.mutable.ListBuffer
import com.hp.hpl.jena.rdf.model._
import scala.collection.mutable
import com.trustlayers.reasoner.airj.JustificationLogger
import com.trustlayers.reasoner.n3.NsName
import scala.util.{Failure, Success, Try}
import com.trustlayers.reasoner.air.air_ns

/**
 * User: jacek
 * Date: 4/17/14
 *
 * The ReasoningEngine implements the COMP-CLOSURE algorithm from http://tw.rpi.edu/web/project/TAMI/AIR_Language_Formalization
 * This implementation uses mutable collections to represent state of the reasoner.
 * These include queues of matching and non-matching rule activations,
 * the list of Jena names of all rule instantiations that were matched against the factbase and didn't match
 * and a map from Jena names to AIR rule activations.
 *
 * It should be possible to come up with an implementation that only uses immutable reasoner state
 *
 * AIR rules can be instantiated: in the process their free variables are replaced by ground terms
 *
 * The IF parts of rules can be translated to Jena syntax for matching against facts
 * When (possibly instantiated) AIR rules are translated to Jena syntax, they are given new, unique String names.
 * These names will then come up in Bindings reported by Jena builtin,
 * can be used to refer back to AIR rules
 * and are used to track the success(matched something)/failure(didn't match anything) status of rules
 *
 *
 *
 * TODO: make sure the newly derived facts are input to more forward-chaining reasoning
 * Notes
 * - rules w/o free variables can safely be omitted from the list of rules to consider for fwd chaining
 * - forward chaining can also activate some new rules
 *
 *
 */

class ReasoningEngine(factsSource: DataSource, airProgram: AirProgram)(implicit jlog: JustificationLogger) {

  // when rule body matches triples in Jena, its variables are set to values
  // these values are stored in the order of variable occurrence in the body
  // they can be later 'zipped' with the variable list
  case class Bindings(ruleName: String, values: IndexedSeq[JenaNode])

  val model = ModelFactory.createDefaultModel()

  private val ruleActivationMap = mutable.Map.empty[String, RuleActivation] // from assigned Jena names to activations

  // bindings reported by Jena builtin
  private val currentRuleBindings = ListBuffer.empty[Bindings]
  // this set is used to determine which rules failed to match
  private val activeJenaRuleNames = mutable.Set.empty[String]

  // successful rules to fire
  private val srf = mutable.Queue.empty[RuleActivation]
  // failed rules to fire else actions
  private val frf = mutable.Queue.empty[RuleActivation]

  private val newTriples = mutable.Set.empty[Term]
  private val ruleBase = ListBuffer.empty[RuleActivation]


  // called from Jena builtin
  private def reportBindings(ruleName: String, values: IndexedSeq[JenaNode]) {
    currentRuleBindings += new Bindings(ruleName, values)
  }

  private class RuleMatchBuiltin extends BaseBuiltin {
    override def getName = builtinName

    override def headAction(args: Array[JenaNode], length: Int, context: RuleContext) {
      reportBindings(context.getRule.getName, (0 until length).map(args(_)))
    }
  }

  // implements true() in Rule body
  private class TrueBuiltin extends BaseBuiltin {
    override def getName = "true"

    override def bodyCall(args: Array[JenaNode], length: Int, context: RuleContext) = true
  }

  def init() {
    // builtins have to be registered before rule parsing
    BuiltinRegistry.theRegistry.register(new RuleMatchBuiltin)
    BuiltinRegistry.theRegistry.register(new TrueBuiltin)

    jlog.startClosureComputation(factsSource.toString)
    model.read(factsSource.in, factsSource.base, factsSource.lang)
    factsSource.close()
    // activate only the top rules at first, with -1 signifying starting nestingDependency
    airProgram.topRules.foreach {
      (rule: Rule) => addToRulebase(createActivation(rule, EvalContext.empty, -1))
    }
  }

  def runLoop() {
    while (srf.nonEmpty || frf.nonEmpty) {
      newTriples.clear()
      while (srf.nonEmpty) {
        fireThenActions(srf.dequeue())
      }
      // copy the 'doesn't match' queue while verifying the rules indeed didn't match
      val frfCopy = frf.filter(activation => activeJenaRuleNames.contains(activation.rule.jenaName))
      frf.clear()
      if (frfCopy.nonEmpty) {
        jlog.closingTheWorld()
      }
      while (frfCopy.nonEmpty) {
        fireElseActions(frfCopy.dequeue())
      }
      if (newTriples.nonEmpty && ruleBase.nonEmpty) {
//        println("new facts")
        rerunActiveRules(newTriples.toSet, ruleBase.toList)
      }
    }
    jlog.closeClosureComputation()
  }

  /**
   * This corresponds closely to add-to-rulebase from AIR papers
   * The method not only just adds the rule but tries to match its IF (body) part
   * using Jena and
   * -- if matches are reported by Jena builtin, corresponding THEN part activations will be added to SRF queue
   * -- if there are no matches (and the rule has an else) an activation will be added to the Failed queue
   *
   * @param ruleActivation contains instantiated rule w/ unique Jena name and bindings in effect at activation
   */

  def addToRulebase(ruleActivation: RuleActivation) {
    // run Jena matching
    currentRuleBindings.clear()
    val rule = ruleActivation.rule
    val active = mutable.Set(rule.jenaName)
    val reasoner = getReasoner(ruleActivation.toJena :: Nil, derivationLogging = false)
    val infModel = ModelFactory.createInfModel(reasoner, model)
    // force reasoner matching
    infModel.prepare()
    // matching in Jena adds bindings to currentRuleBindings via builtin
    currentRuleBindings.foreach {
      binding => {
        ruleActivationMap.get(binding.ruleName) match {
          case Some(RuleActivation(rule1, bindings1, fid)) =>
            val matching = EvalContext(rule1.vars, binding.values)
            srf.enqueue(RuleActivation(rule1, matching, fid))
            active -= rule1.jenaName // this rule matched, so we remove its Jena name from this: what remains is failed
          case None => println("No activation record found for " + binding)
        }
      }
    }
    // active now represents rules that failed matching
    active.foreach {
      jenaName => ruleActivationMap.get(jenaName) match {
        case Some(activation) if activation.rule.hasElse => frf.enqueue(activation)
        case Some(activation) => ()
        case None => println("No activation record found for " + jenaName)
      }
    }
    activeJenaRuleNames ++= active
    if (rule.vars.nonEmpty) {
      // consider only non-ground rules for matching against updated FB
      ruleBase += ruleActivation
    }
  }

  // TODO for now newStatements and newTriples are redundant
  def rerunActiveRules(newTriples: Set[Term], activeRules: List[RuleActivation]) {
    // run Jena matching
    currentRuleBindings.clear()
    //    val rule = ruleActivation.rule
    val active = mutable.Set.empty[String]
    active ++= activeRules.map(_.rule.jenaName)
    val reasoner = getReasoner(activeRules.map(_.toJena), derivationLogging = false)
    val infModel = ModelFactory.createInfModel(reasoner, model)
    // force reasoner matching
    infModel.prepare()
    // matching in Jena adds bindings to currentRuleBindings via builtin
    currentRuleBindings.foreach {
      binding => {
        ruleActivationMap.get(binding.ruleName) match {
          case Some(RuleActivation(rule1, bindings1, fid)) =>
            // TODO check if matching involves new facts
            // TODO need to prune spurious activations at this stage
            // TODO optimization: if only one predicate (non ground) then just match against new facts not whole FB
            // TODO example: IntIncr example: when rule is rerun, it matches all the previously added facts

            val matching = EvalContext(rule1.vars, binding.values)
            val matchedTerms = rule1.body.predicates.collect {
              case term: Term => term.instantiate(matching)
            }

            if (matchedTerms.exists(newTriples.contains)) {
              srf.enqueue(RuleActivation(rule1, matching, fid))
            } else {
//              println("spurious")
            }

            active -= rule1.jenaName // this rule matched, so we remove its Jena name from this: what remains is failed
          case None => println("No activation record found for " + binding)
        }
      }
    }
    // active now represents rules that failed matching
    active.foreach {
      jenaName => ruleActivationMap.get(jenaName) match {
        case Some(activation) if activation.rule.hasElse => frf.enqueue(activation)
        case Some(activation) => ()
        case None => println("No activation record found for " + jenaName)
      }
    }
    activeJenaRuleNames ++= active

    // TODO update rule base appropriately; do we need new Jena names?


    /*
        if (rule.vars.nonEmpty) {
          // consider only non-ground rules for matching against updated FB
          ruleBase += ruleActivation
        }
    */
  }

  def createActivation(rule: Rule, bindings: EvalContext, nestedDependencyId: Int) = {
    // generate a unique name for this activation
    val jenaRuleName = airProgram.nextJenaRuleName
    val instantiatedRule = rule.instantiate(jenaRuleName, bindings)
    val ruleActivation = new RuleActivation(instantiatedRule, bindings, nestedDependencyId)
    ruleActivationMap.put(jenaRuleName, ruleActivation)
    ruleActivation
  }

  private def fireThenActions(ra: RuleActivation) {
    val bindings = ra.bindings
    val rule = ra.rule
    val ruleApplicationData = jlog.ruleFiring(rule, bindings, isThenBranch = true, ra.inputFacts, ra.nestedDependencyId)
    val thenActions = rule.instantiateThenActions(bindings)
    thenActions.foreach {
      action => action.perform(this, bindings, ruleApplicationData)
    }
  }

  private def fireElseActions(ra: RuleActivation) {
    val bindings = ra.bindings
    val rule = ra.rule
    val ruleApplicationData = jlog.ruleFiring(rule, bindings, isThenBranch = false, ra.inputFacts, ra.nestedDependencyId)
    val elseActions = rule.instantiateElseActions(bindings)
    elseActions.foreach {
      action => action.perform(this, bindings, ruleApplicationData)
    }
  }

  // TODO: This can probably become private once we convert tests.
  def createProperty(uri: String, local: String) = model.createProperty(uri, local)

  // TODO: This can probably be eliminated once we turn *Testing into tests which validate results
  def listStatements(model: Model, property: Property) {
    val statements = model.listStatements(null, property, null)
    println("listing facts with property: " + property)
    statements.foreach {
      statement => {
        println(statement)
        println()
      }
    }
    println("----- END -----")
  }

  def complianceStatements = {
    val airCompliantWith = model.createProperty(air_ns, "compliant-with")
    model.listStatements(null, airCompliantWith, null)
  }

  def nonComplianceStatements = {
    val airNonCompliantWith = model.createProperty(air_ns, "non-compliant-with")
    model.listStatements(null, airNonCompliantWith, null)
  }

  private def getReasoner(ruleTextLines: scala.collection.Seq[String], derivationLogging: Boolean = false) = {
    val reasoner = new GenericRuleReasoner(JenaRule.parseRules(JenaRule.rulesParserFromReader(buildReader(ruleTextLines))))
    reasoner.setDerivationLogging(derivationLogging)
    reasoner
  }

  def assert(terms: List[Term], rad: RuleApplicationData) {
    Try(terms.map(_.toStatement(model)).map(_.get)) match {
      case Success(statements) =>
        statements zip terms foreach {
          case (statement, term) => if (model.contains(statement)) {
            println("already known " + statement)
          } else {
            model.add(statement)
            newTriples += term
            rad.assert(term)
          }
        }

      case Failure(ex) => println(ex)
    }
  }

  def description(ruleName: NsName, text: String, rad: RuleApplicationData) {
    rad.addDescription(text)
  }

  // activate rule known by name
  def activateRule(ruleName: NsName, bindings: EvalContext, rad: RuleApplicationData) {
    airProgram.findRule(ruleName) match {
      case Some(rule) => activateRule(rule, bindings, rad)
      case None => println(s"rule $ruleName not found")
    }
  }

  def activateRule(rule: Rule, bindings: EvalContext, rad: RuleApplicationData) {
    val ra = createActivation(rule, bindings, rad.id)
    rad.activate(ra)
    addToRulebase(ra)
  }
}

case class RuleActivation(rule: Rule, bindings: EvalContext = EvalContext.empty, nestedDependencyId: Int) {
  def toJena = rule.toJenaString(bindings)

  def inputFacts = rule.inputFacts(bindings)
}
