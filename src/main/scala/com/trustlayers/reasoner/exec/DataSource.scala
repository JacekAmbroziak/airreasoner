package com.trustlayers.reasoner.exec

import java.io.{InputStreamReader, Closeable, InputStream}

//TODO: Comment on how base & uri get used.
// @lang: Used to define the format of the input stream (rdf/xml, n3, etc)
case class DataSource(in: InputStream, base: String, lang: String, uri: String) extends Closeable {
  def close() {
    in.close()
  }

  def getReader = new InputStreamReader(in)
}
