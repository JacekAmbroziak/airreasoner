package com.trustlayers.reasoner

import com.trustlayers.reasoner.datastore.InMemoryDatastore
import com.hp.hpl.jena.reasoner.rulesys.{RuleContext, BuiltinRegistry}
import com.hp.hpl.jena.reasoner.rulesys.builtins.BaseBuiltin
import com.hp.hpl.jena.graph.{Node_Variable, Node}

/**
 * User: frobnoid
 * Date: 3/25/14
 */
class ReasonerTest extends UnitTest {

  "A Reasoner" should "produce results" in {
    val ds = new InMemoryDatastore
    ds.loadFacts(List("@prefix : <http://x/> . :Jacek :livesIn :Cambridge."))

    val rules = "[sample-rule: (?SUBJ http://x/livesIn ?) -> (?SUBJ " + air_ns + "compliant-with test-passes)]"
    val reasoner = new Reasoner(rules, None, ds.getFactsModelForReasoning)
    reasoner.getCompliance.foreach(assertion => {
      assertion.getSubject.getURI should be("http://x/Jacek")
      println(assertion + " comes from: ")
      reasoner.getDerivation(assertion).foreach(println)
    })
  }

  class TestBuiltin extends BaseBuiltin {
    override def getName = "test-builtin"

    override def bodyCall(args: Array[Node], length: Int, context: RuleContext) = {
      println(s"bodyCall, len=$length")
      val env = context.getEnv
      println(env)
      true
    }

    override def headAction(args: Array[Node], length: Int, context: RuleContext) = {
      println(s"headCall, len=$length")
      val env = context.getEnv
      val node = env.getGroundVersion(new Node_Variable("SUBJ"))
      println(node)
      println(env)
    }

    override def isSafe = false
  }

  "A Reasoner" should "call a builtin" in {
    val ds = new InMemoryDatastore
    ds.loadFacts(List("@prefix : <http://x/> . :Jacek :livesIn :Cambridge."))
    BuiltinRegistry.theRegistry.register(new TestBuiltin)

    val rules = "[sample-rule: (?SUBJ http://x/livesIn ?LOC) test-builtin() -> test-builtin(\"head\") (?SUBJ " + air_ns + "compliant-with test-passes)]"
    val reasoner = new Reasoner(rules, None, ds.getFactsModelForReasoning)
    reasoner.getCompliance.foreach(assertion => {
      assertion.getSubject.getURI should be("http://x/Jacek")
      println(assertion + " comes from: ")
      reasoner.getDerivation(assertion).foreach(println)
    })
  }
}
