package com.trustlayers.reasoner

import scala.collection.JavaConversions._

import com.hp.hpl.jena.rdf.model.{Model, InfModel, ModelFactory}
import com.hp.hpl.jena.reasoner.rulesys.{BuiltinRegistry, RuleContext, GenericRuleReasoner, Rule}
import java.io.{PrintWriter, StringReader, BufferedReader}
import com.hp.hpl.jena.reasoner.Derivation
import com.hp.hpl.jena.reasoner.rulesys.builtins.BaseBuiltin
import com.hp.hpl.jena.graph.Node

/**
 * User: jacek
 * Date: 4/4/14
 */
object JenaTesting extends App {
  println("4/4/14")

  class TestBuiltin extends BaseBuiltin {
    override def getName = "bi"

    override def bodyCall(args: Array[Node], length: Int, context: RuleContext) = {
      println(s"bodyCall, len=$length")
      println(context.getEnv)
      true
    }

    override def headAction(args: Array[Node], length: Int, context: RuleContext) = {
      println(s"headCall, len=$length")
      println(context.getEnv)
      if (length > 0) args foreach println
    }
  }

  class TrueBuiltin extends BaseBuiltin {
    override def getName = "true"

    override def bodyCall(args: Array[Node], length: Int, context: RuleContext) = true
  }

  val ruleTextLines =
    "@prefix air: <http://dig.csail.mit.edu/TAMI/2007/amord/air#>." ::
      "@prefix tamip: <http://tw.rpi.edu/proj/tami/Special:URIResolver/Property-3A>." ::
      "@prefix :  <#> ." ::
      "[r1: (?person tamip:Lives_in_city ?city) (?city tamip:Has_state :NY) bi(\"body\") -> (?person air:compliant-with :ny_state_residency_or_id_policy) bi(\"head\")]" ::
      "[r2: (?person tamip:Has_ny_state_id ?NY_STATE_ID) -> bi() (?person air:compliant-with :ny_state_residency_or_id_policy)]" ::
      "[r3:  greaterThan(100, 70) -> (:hundred :greaterThan :seventy)]" ::
      "[r31: sum(70, 1, ?sum) greaterThan(?sum, 70) -> (:seventyone :greaterThan :seventy)]" ::
      "[r4: true()  -> bi(\"empty\") (:some :thing :matched)]" ::
      "[r5: (:number :example ?n) lessThan(?n, 10) sum(?n, 1, ?oneMore) -> (:number :example ?oneMore)]" ::
      Nil

  def concat(lines: Seq[String]) = lines.foldLeft(new StringBuilder)(_.append(_).append("\n")).toString()

  def buildReader(lines: Seq[String]) = new BufferedReader(new StringReader(concat(lines)))

  def mkStmt(m: Model, subj: String, propUri: String, propLocal: String, obj: String) =
    m.createStatement(m.createResource(subj), m.createProperty(propUri, propLocal), m.createResource(obj))

  def mkStmt(m: Model, subj: String, propUri: String, propLocal: String, obj: Integer) =
    m.createStatement(m.createResource(subj), m.createProperty(propUri, propLocal), m.createTypedLiteral(obj))

  def getReasoner(lines: Seq[String]) = {
    val reasoner = new GenericRuleReasoner(Rule.parseRules(Rule.rulesParserFromReader(buildReader(lines))))
    reasoner.setDerivationLogging(true)
    reasoner
  }

  println(concat(ruleTextLines))

  println()
  println()

  BuiltinRegistry.theRegistry.register(new TestBuiltin)
  BuiltinRegistry.theRegistry.register(new TrueBuiltin)

  val reasoner = getReasoner(ruleTextLines)

  val model = ModelFactory.createDefaultModel()
  model.read(ClassLoader.getSystemResourceAsStream("tutorial/demo.rdf"), "", "RDF")

  model.add(mkStmt(model, "#number", "#", "example", new Integer(0)))


  val infModel: InfModel = ModelFactory.createInfModel(reasoner, model)
  val airCompliantWith = model.createProperty("http://dig.csail.mit.edu/TAMI/2007/amord/air#", "compliant-with")

  val out = new PrintWriter(System.out)

  infModel.listStatements(null, null, null).foreach {
    statement => {
      println(statement)
      val iterator: Iterator[Derivation] = infModel.getDerivation(statement)
      iterator.foreach {
        derivation => {
          println(derivation)
          derivation.printTrace(out, true)
          out.flush()
        }
      }
      println()
    }
  }

  //  inf.listStatements().foreach(println)
}