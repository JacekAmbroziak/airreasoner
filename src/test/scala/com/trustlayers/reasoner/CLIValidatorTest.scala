package com.trustlayers.reasoner


class CLIValidatorTest extends UnitTest {

  "A CLIValidator" should "parse a simple load command (no ontology file specified)" in {
    val opt = new CLIValidator(Array("--datastore", "/tmp/foo", "--factfile", "file"))
    opt.datastore.get should be(Some("/tmp/foo"))
    opt.fact.get should be(Some("file"))
    opt.ont shouldBe empty
    opt.policy shouldBe empty
  }

  "A CLIValidator" should "accept an ontology file" in {
    val opt = new CLIValidator(Array("--datastore", "/tmp/foo", "--factfile", "file", "--ontfile", "ontfile"))
    opt.datastore.get should be(Some("/tmp/foo"))
    opt.fact.get should be(Some("file"))
    opt.ont.get should be(Some("ontfile"))
  }

  "A CLIValidator" should "accept a policy file" in {
    val opt = new CLIValidator(Array("--datastore", "/tmp/foo", "--policyfile", "file"))
    opt.datastore.get should be(Some("/tmp/foo"))
    opt.policy.get should be(Some("file"))
  }

  // TODO: negative test cases
}
