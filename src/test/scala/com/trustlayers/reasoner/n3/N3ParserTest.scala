package com.trustlayers.reasoner.n3

import io.Source
import com.trustlayers.reasoner.UnitTest
import com.trustlayers.reasoner.n3.N3Parser._
import com.trustlayers.reasoner.n3.N3Parser.ExistentialVariables
import com.trustlayers.reasoner.n3.N3Parser.Variable
import scala.util.Success
import com.trustlayers.reasoner.n3.N3Parser.UniversalVariables
import com.trustlayers.reasoner.n3.N3Parser.Properties0
import scala.util.Failure
import com.trustlayers.reasoner.air.IsA

class N3ParserTest extends UnitTest {
  val parser = N3Parser

  def readFromResourceFile(filename: String) = {
    Source.fromURL(getClass.getResource(filename)).reader()
  }

  def qname(pre: String, post: String) = {
    new parser.QName(pre, post)
  }

  def variable(ns: String, v: String) = {
    new parser.Variable0(qname(ns, v))
  }

  def property(q1: parser.QName, q2: parser.QName) = {
    new parser.Property0(q1, q2)
  }

  "A N3Parser" should "parse a qname" in {
    parser.parseAll(parser.qName, "a:b").get should be(qname("a", "b"))
  }

  "A N3Parser" should "parse an empty prefix" in {
    parser.parseAll(parser.prefixDecl, "@prefix : <DEFAULT>.").get should be(parser.PrefixDecl("", "DEFAULT"))
  }

  "A N3Parser" should "parse an air:if" in {
    parser.parseAll(parser.qName, "air:if").get should be(qname("air", "if"))
  }

  "A N3Parser" should "parse a QName, no prefix" in {
    parser.parseAll(parser.qName, "if").get should be(qname("", "if"))
  }

  "A N3Parser" should "parse a variable" in {
    parser.parseAll(parser.variable, ":x").get should be(variable("", "x"))
  }

  "A N3Parser" should "parse universal variables" in {
    parser.parseAll(parser.universalVariables, "@forAll :a, :two, :four.").get should be(parser.UniversalVariables0(List(variable("", "a"), variable("", "two"), variable("", "four"))))
  }

  "A N3Parser" should "parse existential variables" in {
    parser.parseAll(parser.existentialVariables, "@forSome :a, :two, :four.").get should be(parser.ExistentialVariables0(List(variable("", "a"), variable("", "two"), variable("", "four"))))
  }

  "A N3Parser" should "parse properties" in {
    parser.parseAll(parser.properties, "[a b]").get should be(Properties0(List(property(qname("", "a"), qname("", "b")))))
  }

  "A N3Parser" should "parse properties with semi" in {
    parser.parseAll(parser.properties, "[a b; c d]").get should be(Properties0(List(property(qname("", "a"), qname("", "b")), property(qname("", "c"), qname("", "d")))))
  }

  "A N3Parser" should "parse a statement" in {
    parser.parseAll(parser.statement, ":a :b :c .").get should be(List(parser.Statement0(qname("", "a"), qname("", "b"), qname("", "c"))))
  }

  "A N3Parser" should "parse a statement with a string" in {
    parser.parseAll(parser.statement, "a b \"a string\" .").get should be(List(parser.Statement0(qname("", "a"), qname("", "b"), StringLiteral("\"a string\""))))
  }

  "A N3Parser" should "parse a statement with a prefix" in {
    parser.parseAll(parser.statement, "a air:if b .").get should be(List(parser.Statement0(qname("", "a"), qname("air", "if"), qname("", "b"))))
  }

  "A N3Parser" should "parse a statement (really a ruleset)" in {
    parser.parseAll(parser.statement, ":TestPolicy a air:RuleSet; air:rule :air-test-rule .").get should be(List(
      parser.Statement0(qname("", "TestPolicy"), IsA, qname("air", "RuleSet")),
      parser.Statement0(qname("", "TestPolicy"), qname("air", "rule"), qname("", "air-test-rule"))))
  }

  "A N3Parser" should "parse a formula" in {
    parser.parseAll(parser.formula, "{one two three.}").get should be(
      parser.Formula0(Nil, List(parser.Statement0(qname("", "one"), qname("", "two"), qname("", "three")))))
  }

  "A N3Parser" should "parse a formula with multiple statements" in {
    parser.parseAll(parser.formula, "{one two three. four five six.}").get should be(
      parser.Formula0(Nil, List(parser.Statement0(qname("", "one"), qname("", "two"), qname("", "three")),
        parser.Statement0(qname("", "four"), qname("", "five"), qname("", "six")))))
  }

  "A N3Parser" should "parse a statement with formula" in {
    parser.parseAll(parser.statement, "a log:includes {z weather w.}.").get should be(List(parser.Statement0(
      qname("", "a"), qname("log", "includes"), parser.Formula0(Nil, List(parser.Statement0(qname("", "z"), qname("", "weather"), qname("", "w")))))))
  }

  val nsmap = Map("defined" -> "http://somevalue.com/", "" -> "http://emptystring.com/")

  "A QName" should "successfully resolve a NS" in {
    val nsname = qname("defined", "value").resolve(nsmap)
    nsname should be(Success(NsName("http://somevalue.com/", "value")))
  }

  "A QName" should "not resolve a missing NS" in {
    val nsname = qname("undefined", "value").resolve(nsmap)
    nsname.toString should be(Failure(new Exception("unknown prefix 'undefined', local: 'value'")).toString)
  }

  "A QName" should "resolve the empty NS" in {
    val nsname = qname("", "value").resolve(nsmap)
    nsname should be(Success(NsName("http://emptystring.com/", "value")))
  }

  // TODO: This is a valid test case which is failing!
  /*
  "A QName" should "already dereferenced should succeed" in {
    val nsname = qname("http://foobar.com/", "value").resolve(nsmap)
    nsname should be (Success(NsName("http://foobar.com/", "value")))
  }
  */

  "Universal Variables" should "resolve their variables" in {
    val uvars = parser.UniversalVariables0(List(variable("", "a"), variable("defined", "two"))).resolve(nsmap)
    uvars should be(Success(UniversalVariables(List(Variable(NsName("http://emptystring.com/", "a")), Variable(NsName("http://somevalue.com/", "two"))))))
  }

  "Universal Variables" should "provide appropriate errors on failure" in {
    val uvars = parser.UniversalVariables0(List(variable("", "a"), variable("undefined", "two"))).resolve(nsmap)
    uvars.toString should be(Failure(new Exception("unknown prefix 'undefined', local: 'two'")).toString)
  }

  "Existential Variables" should "resolve their variables" in {
    val xvars = parser.ExistentialVariables0(List(variable("", "a"), variable("defined", "two"))).resolve(nsmap)
    xvars should be(Success(ExistentialVariables(List(Variable(NsName("http://emptystring.com/", "a")), Variable(NsName("http://somevalue.com/", "two"))))))
  }

  "Existential Variables" should "provide appropriate errors on failure" in {
    val xvars = parser.ExistentialVariables0(List(variable("", "a"), variable("undefined", "two"))).resolve(nsmap)
    xvars.toString should be(Failure(new Exception("unknown prefix 'undefined', local: 'two'")).toString)
  }

  "Statement" should "resolve correctly" in {
    val statement = parser.Statement0(QName("", "a"), QName("", "b"), QName("defined", "c")).resolve(nsmap)
    statement should be(Success(Statement(NsName("http://emptystring.com/", "a"), NsName("http://emptystring.com/", "b"), NsName("http://somevalue.com/", "c"))))
  }

  "Statement" should "provide appropriate errors on failure" in {
    val statement = parser.Statement0(QName("undefined", "a"), QName("", "b"), QName("", "c")).resolve(nsmap)
    statement.toString should be(Failure(new Exception("unknown prefix 'undefined', local: 'a'")).toString)
  }

  "Property" should "provide appropriate errors on failure" in {
    val property = parser.Property0(QName("defined", "a"), QName("undefined", "b")).resolve(nsmap)
    property.toString should be(Failure(new Exception("unknown prefix 'undefined', local: 'b'")).toString)
  }

  "Property" should "resolve correctly" in {
    val property = parser.Property0(QName("defined", "a"), QName("", "b")).resolve(nsmap)
    property should be(Success(Property(NsName("http://somevalue.com/", "a"), NsName("http://emptystring.com/", "b"))))
  }

  "Formula" should "resolve correctly" in {
    val formula = parser.Formula0(Nil, List(Statement0(QName("", "a"), QName("", "b"), QName("defined", "c")))).resolve(nsmap)
    formula should be(Success(Formula(Nil, Statements(List(Statement(NsName("http://emptystring.com/", "a"), NsName("http://emptystring.com/", "b"), NsName("http://somevalue.com/", "c")))))))
  }

  "Formula" should "provide appropriate errors on failure" in {
    val formula = parser.Formula0(Nil, List(Statement0(QName("undefined", "a"), QName("", "b"), QName("", "c")))).resolve(nsmap)
    formula.toString should be(Failure(new Exception("unknown prefix 'undefined', local: 'a'")).toString)
  }

  // TODO: Should we test for comments?
}
