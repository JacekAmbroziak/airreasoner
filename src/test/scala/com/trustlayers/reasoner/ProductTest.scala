package com.trustlayers.reasoner

import com.trustlayers.reasoner.exec._
import com.trustlayers.reasoner.airj.JustificationLogger
import com.trustlayers.reasoner.exec.DataSource
import com.trustlayers.reasoner.air._
import com.trustlayers.reasoner.exec.DataSource

/**
 * Created by frobnoid on 5/16/14.
 */
class ProductTest extends UnitTest {

  /*
   * Run a test case and return the engine
   */
  def runEngine(rules: String, facts: String) = {
    implicit val jlog = new JustificationLogger

    val factsSource = dataSourceFromFile(facts)
    val rulesSource = dataSourceFromFile(rules)

    val airProgram = AirProgram.parse(rulesSource).get
    val engine = new ReasoningEngine(factsSource, airProgram)

    engine.init()
    engine.runLoop()

    jlog.storeLog()
    engine
  }
}
