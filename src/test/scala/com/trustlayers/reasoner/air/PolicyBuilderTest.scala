package com.trustlayers.reasoner.air

import com.trustlayers.reasoner.UnitTest
import com.trustlayers.reasoner.n3.N3Parser._
import com.trustlayers.reasoner.n3.{UniversalVariable, NamedResource, NsName}
import scala.collection.mutable.ListBuffer
import scala.collection.immutable.Map

/**
 * User: frobnoid
 * Date: 3/25/14
 */
class PolicyBuilderTest extends UnitTest {
  val ruleName = new NsName("http://air-test/", "ruleName")
  val ruleSetName = new NsName("http://air-test/", "ruleSetName")

  def process(input: TopLevel) : PolicyBuilder = {
    process(List(input), Map.empty[NsName,NamedResource])
  }

  def process(input: List[TopLevel], map: Map[NsName,NamedResource]) = {
    input.foldLeft(PolicyBuilder(map))(_.processTopLevel(_).get)
  }

  "A PolicyBuilder" should "create a belief rule" in {
    val input = new Statement(ruleName, IsA, AirBeliefRuleResource)
    process(input).resources(ruleName) should be(new BeliefRule(ruleName))
  }

  "A PolicyBuilder" should "create a hidden rule" in {
    val input = new Statement(ruleName, IsA, AirHiddenRuleResource)
    process(input).resources(ruleName) should be(new HiddenRule(ruleName))
  }

  "A PolicyBuilder" should "create a elided rule" in {
    val input = new Statement(ruleName, IsA, AirElidedRuleResource)
    process(input).resources(ruleName) should be(new ElidedRule(ruleName))
  }

  "A PolicyBuilder" should "create a rule set" in {
    val input = new Statement(ruleSetName, IsA, AirRuleSetResource)
    process(input).resources(ruleSetName) should be(new AirRuleSet(ruleSetName))
  }

  "A PolicyBuilder" should "should silently accept arbitrary statements" in {
    val input = new Statement(NsName("dummy","value"), NsName("dummy","value"), NsName("dummy","value"))
    process(input).resources.size should be (0)
  }

  "A PolicyBuilder" should "should silently accept arbitrary other top level nodes" in {
    val input = new PrefixDecl("arbitrary", "toplevel")
    process(input).resources.size should be (0)
  }

  // TODO: Turn asInstance calls below into a function which a safe cast mechanism (via match)

  "A PolicyBuilder" should "create a rule set and a rule" in {
    val map = Map(ruleName -> new BeliefRule(ruleName), ruleSetName -> new AirRuleSet(ruleSetName))
    val input = new Statement(ruleSetName, AirRuleRelation, ruleName)
    process(List(input), map).resources(ruleSetName).asInstanceOf[AirRuleSet].ruleNames should be (ListBuffer(ruleName))
  }

  "A PolicyBuilder" should "create an air if" in {
    val map = Map(ruleName -> new BeliefRule(ruleName))
    val nsname = NsName("dummy","dummy")
    val statement = Statement(nsname, nsname, nsname)
    val input = new Statement(ruleName, AirIf, statement)
    process(List(input), map).resources(ruleName).asInstanceOf[AirRule].ifClause should be (statement)
  }

  "A PolicyBuilder" should "create an air else" in {
    val map = Map(ruleName -> new BeliefRule(ruleName))
    val nsname = NsName("dummy","dummy")
    val nsname2 = NsName("ignored","ignored")
    val statement = Statement(nsname, nsname, nsname)
    val statement2 = Statement(nsname2, nsname2, nsname2)
    val input = new Statement(ruleName, AirElse, statement)
    val input2 = new Statement(ruleName, AirElse, statement2)
    process(List(input, input2), map).resources(ruleName).asInstanceOf[AirRule].elseClauses should be (ListBuffer(statement,statement2))
  }

  "A PolicyBuilder" should "create an air then" in {
    val map = Map(ruleName -> new BeliefRule(ruleName))
    val nsname = NsName("dummy","dummy")
    val nsname2 = NsName("ignored","ignored")
    val statement = Statement(nsname, nsname, nsname)
    val statement2 = Statement(nsname2, nsname2, nsname2)
    val input = new Statement(ruleName, AirThen, statement)
    val input2 = new Statement(ruleName, AirThen, statement2)
    process(List(input, input2), map).resources(ruleName).asInstanceOf[AirRule].thenClauses should be (ListBuffer(statement,statement2))
  }

  "A PolicyBuilder" should "Add a comment to a Rule" in {
    val map = Map(ruleName -> new BeliefRule(ruleName))
    val input = new Statement(ruleName, RdfsComment, new StringLiteral("comment"))
    process(List(input), map).resources(ruleName).asInstanceOf[AirRule].comment should be ("comment")
  }

  "A PolicyBuilder" should "Add a label to a Rule" in {
    val map = Map(ruleName -> new BeliefRule(ruleName))
    val input = new Statement(ruleName, RdfsLabel, new StringLiteral("label"))
    process(List(input), map).resources(ruleName).asInstanceOf[AirRule].label should be ("label")
  }

  "A PolicyBuilder" should "Add a comment to a RuleSet" in {
    val map = Map(ruleSetName -> new AirRuleSet(ruleSetName))
    val input = new Statement(ruleSetName, RdfsComment, new StringLiteral("comment"))
    process(List(input), map).resources(ruleSetName).asInstanceOf[AirRuleSet].comment should be ("comment")
  }

  "A PolicyBuilder" should "Add a label to a RuleSet" in {
    val map = Map(ruleSetName -> new AirRuleSet(ruleSetName))
    val input = new Statement(ruleSetName, RdfsLabel, new StringLiteral("label"))
    process(List(input), map).resources(ruleSetName).asInstanceOf[AirRuleSet].label should be ("label")
  }

  "A PolicyBuilder" should "Add universal variables" in {
    val nsone = NsName("ns","one")
    val nstwo = NsName("ns","two")

    val uvs = new UniversalVariables(List(Variable(nsone),Variable(nstwo)))
    val input = new Statement(ruleSetName, RdfsLabel, new StringLiteral("label"))
    val resources = process(uvs).resources
    resources(nsone).asInstanceOf[UniversalVariable] should be (UniversalVariable(nsone))
    resources(nstwo).asInstanceOf[UniversalVariable] should be (UniversalVariable(nstwo))
  }

  // TODO: Tests for nested rules
}
