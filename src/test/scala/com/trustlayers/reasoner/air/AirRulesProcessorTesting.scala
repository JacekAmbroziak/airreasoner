package com.trustlayers.reasoner.air

import java.io.InputStreamReader
import scala.util.{Failure, Success}

object AirRulesProcessorTesting extends App {
  def resourceReader(name: String) = new InputStreamReader(ClassLoader.getSystemResourceAsStream(name))

  def extractAirRules(resourceName: String) = {
    println(resourceName)
    AirRulesProcessor.extractAirResources(resourceReader(resourceName)) match {
      case Success(resources) => println("SUCCESS")
      case Failure(ex) =>
        println("FAIL: " + ex)
    }
  }

  extractAirRules("MGL_6-172_cleaned.n3")

  extractAirRules("no_facts/assertion.n3")
  extractAirRules("no_facts/noncompliant.n3")
  extractAirRules("no_facts/beliefRule.n3")

  extractAirRules("tutorial/policy1.n3")
  extractAirRules("tutorial/policy2.n3")
  extractAirRules("tutorial/policy3.n3")
  extractAirRules("tutorial/policy4.n3")
  extractAirRules("tutorial/policy5.n3")
  extractAirRules("tutorial/policy6.n3")
  //    extractAirRules("tutorial/policy7.n3") //  local @forAll
  extractAirRules("tutorial/policy8.n3")
  extractAirRules("tutorial/policy11.n3")
  extractAirRules("tutorial/policy12.n3")
  extractAirRules("tutorial/policy13.n3")
  extractAirRules("tutorial/policy16.n3")
  extractAirRules("tutorial/policy19.n3")
  extractAirRules("tutorial/policy21.n3")
  extractAirRules("tutorial/sky_color.n3")
  extractAirRules("tutorial/justifies.n3")
  extractAirRules("example.n3")
  extractAirRules("gang-policies.n3")
  extractAirRules("saladPolicy.n3")
}
