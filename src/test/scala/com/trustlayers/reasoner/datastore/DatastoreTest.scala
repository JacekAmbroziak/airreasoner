package com.trustlayers.reasoner.datastore

import org.w3.banana._
import com.trustlayers.reasoner.UnitTest

/**
 * User: frobnoid
 * Date: 3/25/14
 */
class DatastoreTest extends UnitTest {

  "A Load" should "be queryable" in {
    val ds = new InMemoryDatastore

    ds.loadFacts(List("@prefix : <http://example> . :foo :bar :baz."))

    val rs = ds.SPARQLQuery("SELECT ?subj where { graph ?g { ?subj ?pred ?obj.} }")

    var rowCount = 0
    rs.foreach(row => {
      row.get("subj").toString should be("http://examplefoo")
      rowCount += 1
    })
    rowCount should be(1)
  }

  "A Load" should "allow two discrete loads" in {
    val ds = new InMemoryDatastore
    ds.loadFacts(List("@prefix : <http://example> . :foo :bar :baz.",
      "@prefix : <http://example> . :foo :buzz :baz.")
    )

    val rs = ds.SPARQLQuery("SELECT ?subj where { graph ?g { ?subj ?pred ?obj.} }")

    var rowCount = 0
    rs.foreach(row => {
      row.get("subj").toString should be("http://examplefoo")
      rowCount += 1
    })
    rowCount should be(2)
  }

  "A datastore" should "allow new triples to be asserted" in {
    val ds = new InMemoryDatastore

    ds.loadFacts(List("@prefix : <http://example> . :foo :bar :baz."))
    ds.assertNewTriple("http://examplefoo", "two", "three", "Unique-ID").getOrFail()

    val rs = ds.SPARQLQuery("SELECT ?subj where { graph ?g { ?subj ?pred ?obj.} }")

    var rowCount = 0
    rs.foreach(row => {
      row.get("subj").toString should be("http://examplefoo")
      rowCount += 1
    })
    rowCount should be(2)
  }

  "A datastore" should "allow triples to be removed" in {
    val ds = new InMemoryDatastore

    ds.loadFacts(List("@prefix : <http://example> . :foo :bar :baz."))
    ds.load("@prefix : <http://example> . :foo :buzz :baz.", "ThisDataToBeDeleted")

    var rs = ds.SPARQLQuery("SELECT ?subj where { graph ?g { ?subj ?pred ?obj.} }")

    var rowCount = 0
    rs.foreach(row => {
      row.get("subj").toString should be("http://examplefoo")
      rowCount += 1
    })
    rowCount should be(2)

    ds.delete("ThisDataToBeDeleted")
    rs = ds.SPARQLQuery("SELECT ?subj where { graph ?g { ?subj ?pred ?obj.} }")

    rowCount = 0
    rs.foreach(row => {
      row.get("subj").toString should be("http://examplefoo")
      rowCount += 1
    })
    rowCount should be(1)
  }
}
