package com.trustlayers.reasoner

import org.scalatest._
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith

/**
 * Created by frobnoid on 3/25/14.
 */
@RunWith(classOf[JUnitRunner])
abstract class UnitTest extends FlatSpec with Matchers with
OptionValues with Inside with Inspectors with PrivateMethodTester
