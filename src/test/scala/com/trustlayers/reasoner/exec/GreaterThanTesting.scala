package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.airj.JustificationLogger

/**
 * User: jacek
 * Date: 4/17/14
 */

object GreaterThanTesting extends App {
  implicit val jlog = new JustificationLogger

  val factsSource = dataSourceFromResource("tutorial/demo.rdf")
  val rulesSource = dataSourceFromResource("no_facts/greaterThan.n3")

  val airProgram = AirProgram.parse(rulesSource).get
  val engine = new ReasoningEngine(factsSource, airProgram)

  engine.init()
  engine.runLoop()

  val model = engine.model
  val greaterThen = model.createProperty("#", "greaterThan")

  engine.listStatements(model, greaterThen)

  jlog.storeLog()
}
