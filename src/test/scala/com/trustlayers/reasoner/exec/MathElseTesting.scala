package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.airj.JustificationLogger
import com.trustlayers.reasoner.air.air_ns

object MathElseTesting extends App {
  implicit val jlog = new JustificationLogger

  val factsSource = dataSourceFromResource("tutorial/demo.rdf")
  val rulesSource = dataSourceFromResource("no_facts/else.n3")

  val airProgram = AirProgram.parse(rulesSource).get
  val engine = new ReasoningEngine(factsSource, airProgram)

  engine.init()
  engine.runLoop()

  val model = engine.model
  val airCompliantWith = model.createProperty(air_ns, "compliant-with")
  val airNonCompliantWith = model.createProperty(air_ns, "non-compliant-with")

  engine.listStatements(model, airCompliantWith)
  engine.listStatements(model, airNonCompliantWith)

  jlog.storeLog()
}
