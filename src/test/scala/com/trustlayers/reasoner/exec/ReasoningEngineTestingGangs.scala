package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.airj.JustificationLogger
import com.trustlayers.reasoner.air_ns

object ReasoningEngineTestingGangs extends App {
  implicit val jlog = new JustificationLogger

  val factsSource = dataSourceFromFile("/tmp/gangs-data.n3")
  val rulesSource = dataSourceFromFile("/tmp/current-policies.n3")

  val airProgram = AirProgram.parse(rulesSource).get
  val engine = new ReasoningEngine(factsSource, airProgram)

  engine.init()
  engine.runLoop()

  val model = engine.model
  val airCompliantWith = model.createProperty(air_ns, "compliant-with")
  val airNonCompliantWith = model.createProperty(air_ns, "non-compliant-with")

  engine.listStatements(model, airCompliantWith)
  engine.listStatements(model, airNonCompliantWith)
}
