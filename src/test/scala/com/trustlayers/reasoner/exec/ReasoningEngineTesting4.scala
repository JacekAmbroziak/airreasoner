package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.airj.JustificationLogger
import com.trustlayers.reasoner.air_ns

object ReasoningEngineTesting4 extends App {
  implicit val jlog = new JustificationLogger

  val factsSource = dataSourceFromResource("tutorial/demo.rdf")
  val rulesSource = dataSourceFromResource("tutorial/policy4.n3")

  val airProgram = AirProgram.parse(rulesSource).get
  val engine = new ReasoningEngine(factsSource, airProgram)

  engine.init()
  engine.runLoop()

  val model = engine.model
  val airCompliantWith = model.createProperty(air_ns, "compliant-with")
  val airNonCompliantWith = model.createProperty(air_ns, "non-compliant-with")

  engine.listStatements(model, airCompliantWith)
  engine.listStatements(model, airNonCompliantWith)
  jlog.storeLog()
}
