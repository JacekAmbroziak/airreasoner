package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.airj.JustificationLogger
import com.trustlayers.reasoner.air.air_ns

/**
 * User: jacek
 * Date: 4/17/14
 */

object ReasoningEngineTesting extends App {
  implicit val jlog = new JustificationLogger

  val factsSource = dataSourceFromResource("tutorial/demo.rdf")
  val rulesSource = dataSourceFromResource("no_facts/nested.n3")

  val airProgram = AirProgram.parse(rulesSource).get
  val engine = new ReasoningEngine(factsSource, airProgram)

  engine.init()
  engine.runLoop()

  val model = engine.model
  engine.listStatements(model, model.createProperty("#", "is"))

  jlog.storeLog()
}
