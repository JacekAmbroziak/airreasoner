package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.airj.JustificationLogger
import com.trustlayers.reasoner.air._

/**
 * User: jacek
 * Date: 4/17/14
 */

object EmptyFormulaTesting extends App {
  implicit val jlog = new JustificationLogger

  val factsSource = dataSourceFromResource("tutorial/demo.rdf")
  val rulesSource = dataSourceFromResource("no_facts/assertion.n3")

  val airProgram = AirProgram.parse(rulesSource).get
  val engine = new ReasoningEngine(factsSource, airProgram)

  engine.init()
  engine.runLoop()

  val model = engine.model
  val airCompliantWith = model.createProperty(air_ns, "compliant-with")

  engine.listStatements(model, airCompliantWith)

  jlog.storeLog()
}
