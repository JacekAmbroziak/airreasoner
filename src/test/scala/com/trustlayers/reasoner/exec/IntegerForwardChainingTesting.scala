package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner.airj.JustificationLogger
import com.trustlayers.reasoner.air.air_ns

object IntegerForwardChainingTesting extends App {
  implicit val jlog = new JustificationLogger

  val factsSource = dataSourceFromResource("integers_data.n3")
  val rulesSource = dataSourceFromResource("integers.n3")

  val airProgram = AirProgram.parse(rulesSource).get
  val engine = new ReasoningEngine(factsSource, airProgram)

  engine.init()
  engine.runLoop()

  val model = engine.model
  val exampleProperty = model.createProperty(air_ns, "compliant-with")

  engine.listStatements(model, exampleProperty)

  jlog.storeLog()
}
