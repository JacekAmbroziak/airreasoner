package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner._
import scala.collection.JavaConversions._
import com.hp.hpl.jena.rdf.model.Model
import com.trustlayers.reasoner.airj.JustificationLogger

/**
 * User: jacek
 * Date: 4/27/14
 */

class ReasoningEngineTest extends UnitTest {
  val ns = "http://tw.rpi.edu/proj/tami/Special:URIResolver/"
  implicit val jlog = new JustificationLogger

  def uri(local: String) = ns + local

  def mkStmt(m: Model, subj: String, propUri: String, propLocal: String, obj: String) =
    m.createStatement(m.createResource(subj), m.createProperty(propUri, propLocal), m.createResource(obj))

  def mkStmtLit(m: Model, subj: String, propUri: String, propLocal: String, obj: Object) =
    m.createStatement(m.createResource(subj), m.createProperty(propUri, propLocal), m.createTypedLiteral(obj))

  def run(policy: String): ReasoningEngine = run(policy, "tutorial/demo.rdf")

  def run(policy: String, facts: String): ReasoningEngine = {
    val factsSource = dataSourceFromResource(facts)
    val rulesSource = dataSourceFromResource(policy)
    val airProgram = AirProgram.parse(rulesSource).get
    val engine = new ReasoningEngine(factsSource, airProgram)
    engine.init()
    engine.runLoop()
    jlog.storeLog()
    engine
  }

  "A ReasoningEngine" should "derive facts with policy1" in {
    val engine = run("tutorial/policy1.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 2
    statements should contain(mkStmt(m, uri("Alice"), air_ns, "compliant-with", uri("ny_state_residency_policy")))
    statements should contain(mkStmt(m, uri("Bob"), air_ns, "compliant-with", uri("ny_state_residency_policy")))
  }

  "A ReasoningEngine" should "derive facts with policy3" in {
    val engine = run("tutorial/policy3.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 1
    statements should contain(mkStmt(m, uri("Alice"), air_ns, "compliant-with", uri("ny_state_residency_and_id_policy")))
  }

  "A ReasoningEngine" should "derive facts with policy4" in {
    val engine = run("tutorial/policy4.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 1
    statements(0) should be(mkStmt(m, uri("George"), air_ns, "compliant-with", uri("ny_neighbor_state_residency_policy")))
  }

  "A ReasoningEngine" should "derive facts with policy5" in {
    val engine = run("tutorial/policy5.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 3
    statements should contain(mkStmt(m, uri("Alice"), air_ns, "compliant-with", uri("ny_state_residency_or_id_policy")))
    statements should contain(mkStmt(m, uri("Bob"), air_ns, "compliant-with", uri("ny_state_residency_or_id_policy")))
    statements should contain(mkStmt(m, uri("David"), air_ns, "compliant-with", uri("ny_state_residency_or_id_policy")))
  }

  "A ReasoningEngine" should "derive facts with policy6" in {
    val engine = run("tutorial/policy6.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 2
    statements should contain(mkStmt(m, uri("Alice"), air_ns, "compliant-with", uri("ny_state_residency_policy")))
    statements should contain(mkStmt(m, uri("Bob"), air_ns, "compliant-with", uri("ny_state_residency_policy")))
  }

  "A ReasoningEngine" should "derive facts with policy11" in {
    val engine = run("tutorial/policy11.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 2
    statements should contain(mkStmt(m, uri("Alice"), air_ns, "compliant-with", uri("ny_state_residency_policy")))
    statements should contain(mkStmt(m, uri("Bob"), air_ns, "compliant-with", uri("ny_state_residency_policy")))
  }

  "A ReasoningEngine" should "derive facts with policy12" in {
    val engine = run("tutorial/policy12.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 1
    statements should contain(mkStmt(m, uri("Alice"), air_ns, "compliant-with", uri("ny_state_residency_and_id_policy")))
  }

  "A ReasoningEngine" should "derive facts with policy16" in {
    val engine = run("tutorial/policy16.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val airNonCompliantWith = m.createProperty(air_ns, "non-compliant-with")
    val compliant = m.listStatements(null, airCompliantWith, null).toList

    compliant should have length 2
    compliant should contain(mkStmt(m, uri("Alice"), air_ns, "compliant-with", uri("ny_state_residency_policy")))
    compliant should contain(mkStmt(m, uri("Bob"), air_ns, "compliant-with", uri("ny_state_residency_policy")))

    val nonCompliant = m.listStatements(null, airNonCompliantWith, null).toList

    nonCompliant should have length 1
    nonCompliant should contain(mkStmt(m, uri("George"), air_ns, "non-compliant-with", uri("ny_state_residency_policy")))
  }

  "A ReasoningEngine" should "derive facts with policy19" in {
    val engine = run("tutorial/policy19.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val compliant = m.listStatements(null, airCompliantWith, null).toList

    compliant should have length 4
    compliant should contain(mkStmt(m, uri("Alice"), air_ns, "compliant-with", uri("ny_state_residency_policy")))
    compliant should contain(mkStmt(m, uri("Bob"), air_ns, "compliant-with", uri("ny_state_residency_policy")))
    compliant should contain(mkStmt(m, uri("Alice"), air_ns, "compliant-with", uri("ny_state_id_policy")))
    compliant should contain(mkStmt(m, uri("David"), air_ns, "compliant-with", uri("ny_state_id_policy")))
  }

  "A ReasoningEngine" should "run trivial assertion rule once" in {
    val engine = run("no_facts/assertion.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#foo", air_ns, "compliant-with", "#bar"))
  }

  "A ReasoningEngine" should "see if 100 > 70" in {
    val engine = run("no_facts/greaterThan.n3")
    val m = engine.model
    val comp = m.createProperty("#", "greaterThan")
    val statements = m.listStatements(null, comp, null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#hundred", "#", "greaterThan", "#seventy"))
  }

  "A ReasoningEngine" should "agree that 1 < 2" in {
    val engine = run("no_facts/lessThan.n3")
    val m = engine.model
    val comp = m.createProperty("#", "lessThan")
    val statements = m.listStatements(null, comp, null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#one", "#", "lessThan", "#two"))
  }

  "A ReasoningEngine" should "verify that 7 == 7" in {
    val engine = run("no_facts/equalTo.n3")
    val m = engine.model
    val statements = m.listStatements(null, m.createProperty("#", "equalTo"), null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#seven", "#", "equalTo", "#seven"))
  }

  "A ReasoningEngine" should "verify that 2 != 3" in {
    val engine = run("no_facts/notEqualTo.n3")
    val m = engine.model
    val statements = m.listStatements(null, m.createProperty("#", "notEqualTo"), null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#two", "#", "notEqualTo", "#three"))
  }

  "A ReasoningEngine" should "demonstrate higher mathematics talents" in {
    val engine = run("no_facts/higherMath.n3")
    val m = engine.model
    val statements = m.listStatements(null, m.createProperty("#", "equals"), null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#two-plus-two", "#", "equals", "#four"))
  }

  "A ReasoningEngine" should "subtract numbers" in {
    val engine = run("no_facts/higherMath2.n3")
    val m = engine.model
    val statements = m.listStatements(null, m.createProperty("#", "equals"), null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#seven-minus-four", "#", "equals", "#three"))
  }

  "A ReasoningEngine" should "run else.n3 correctly" in {
    val engine = run("no_facts/else.n3")
    val m = engine.model
    val statements = m.listStatements(m.createResource("#sample"), null, null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#sample", air_ns, "non-compliant-with", "#broken_math"))
  }

  "A ReasoningEngine" should "run equalTo.n3 correctly" in {
    val engine = run("no_facts/equalTo.n3")
    val m = engine.model
    val statements = m.listStatements(m.createResource("#seven"), null, null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#seven", "#", "equalTo", "#seven"))
  }

  "A ReasoningEngine" should "run nested.n3 correctly" in {
    val engine = run("no_facts/nested.n3")
    val m = engine.model
    val statements = m.listStatements(null, m.createProperty("#is"), null).toList

    statements should have length 1
    statements should contain(mkStmt(m, "#this-rule", "#", "is", "#deeply-nested"))
  }

  "A ReasoningEngine" should "figure out salad ingredients" in {
    val engine = run("saladPolicy.n3", "facts.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 3
    statements should contain(mkStmt(m, "http://salad/apple", air_ns, "compliant-with", air_ns + "red-or-green-food"))
    statements should contain(mkStmt(m, "http://salad/strawberry", air_ns, "compliant-with", air_ns + "red-or-green-food"))
    statements should contain(mkStmt(m, "http://salad/spinach", air_ns, "compliant-with", air_ns + "red-or-green-food"))
  }

  "A ReasoningEngine" should "be able to count to ten" in {
    val engine = run("integers.n3", "integers_data.n3")
    val m = engine.model
    val airCompliantWith = m.createProperty(air_ns, "compliant-with")
    val statements = m.listStatements(null, airCompliantWith, null).toList

    statements should have length 11
    statements should contain(mkStmtLit(m, "http://www.sun.com/#number", air_ns, "compliant-with", new Integer(1)))
    statements should contain(mkStmtLit(m, "http://www.sun.com/#number", air_ns, "compliant-with", new Integer(2)))
    statements should contain(mkStmtLit(m, "http://www.sun.com/#number", air_ns, "compliant-with", new Integer(3)))
    statements should contain(mkStmtLit(m, "http://www.sun.com/#number", air_ns, "compliant-with", new Integer(4)))
    statements should contain(mkStmtLit(m, "http://www.sun.com/#number", air_ns, "compliant-with", new Integer(10)))
  }
}
