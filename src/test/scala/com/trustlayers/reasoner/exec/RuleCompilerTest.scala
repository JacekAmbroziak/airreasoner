package com.trustlayers.reasoner.exec

import com.trustlayers.reasoner._
import com.trustlayers.reasoner.air._
import com.trustlayers.reasoner.n3.N3Parser._
import com.trustlayers.reasoner.n3.{N3Parser, NsName}
import com.trustlayers.reasoner.n3.N3Parser.Statement
import com.trustlayers.reasoner.n3.NsName
import com.trustlayers.reasoner.n3.N3Parser.Formula
import com.trustlayers.reasoner.n3.N3Parser.Statements
import com.trustlayers.reasoner.n3.N3Parser.Properties
import com.trustlayers.reasoner.air.BeliefRule
import com.trustlayers.reasoner.air.AirRuleSet

import com.trustlayers.reasoner.n3.N3Parser.Property

class RuleCompilerTest extends UnitTest {

  val ruleSetName = NsName("rule", "set")
  val ruleSetName2 = NsName("rule", "set2")
  val ruleName = NsName("rule", "name")

  // dummy gets translated into dummyUriRef by the RuleCompiler
  val dummy = NsName("ignored", "ignored")
  val dummyUriRef = UriRef("ignoredignored")


  "A RuleCompiler" should "translate empty rulesets" in {
    def emptyRuleSet(name: NsName) = {
      new RuleSet(name, "", List.empty[NsName])
    }

    val ruleSet = new AirRuleSet(ruleSetName)
    val ruleSet2 = new AirRuleSet(ruleSetName2)
    val map = Map(ruleSetName -> ruleSet, ruleSetName2 -> ruleSet2)

    val program = RuleCompiler.translate(map, "some-uri")

    program.get.ruleSets.size should be(2)
    program.get.ruleSets.toList should be(List(emptyRuleSet(ruleSetName), emptyRuleSet(ruleSetName2)))
  }

  def emptyRuleHead() = {
    new RuleHead(Nil)
  }

  "A RuleCompiler" should "translate an empty rule" in {
    val rule = new BeliefRule(ruleName)
    rule.setIfClause(Formula(Nil, Statements(Nil)))

    val program = RuleCompiler.translate(Map(ruleName -> rule), "some-uri")

    program.get.ruleBuffer.size should be(1)
    val rule1 = program.get.findRule(ruleName).get
    val rule2 = new Rule(ruleName, "", "", Nil, new RuleBody(List(True)), emptyRuleHead(), emptyRuleHead())

    rule1 should be (rule2)
  }


  "A RuleCompiler" should "translate an simple rule" in {
    val rule = new BeliefRule(ruleName)

    rule.setIfClause(Formula(Nil, Statements(List(Statement(dummy,dummy,dummy)))))

    val map = Map(ruleName -> rule)

    val program = RuleCompiler.translate(map, "some-uri")

    program.get.ruleBuffer.size should be(1)
    val rule1 = program.get.findRule(ruleName).get
    val rule2 = new Rule(ruleName, "", "", Nil, new RuleBody(List(Term(dummyUriRef,dummyUriRef,dummyUriRef))),
      emptyRuleHead(), emptyRuleHead())

    rule1 should be (rule2)
  }


  "A RuleCompiler" should "translate a rule with an else" in {
    val rule = new BeliefRule(ruleName)
    rule.setIfClause(Formula(Nil, Statements(Nil)))
    rule.addElseClause(Properties(List(Property(AirAssert,Formula(Nil, Statements(List(Statement(dummy,dummy,dummy))))))))

    val program = RuleCompiler.translate(Map(ruleName -> rule), "some-uri")

    program.get.ruleBuffer.size should be(1)
    val rule1 = program.get.findRule(ruleName).get
    val rule2 = new Rule(ruleName, "", "", Nil, new RuleBody(List(True)), emptyRuleHead(),
      new RuleHead(List(new AssertAction(List(Term(dummyUriRef,dummyUriRef,dummyUriRef))))))

    rule1 should be (rule2)
  }

  "A RuleCompiler" should "translate a rule with multiple thens and elses" in {
    val rule = new BeliefRule(ruleName)
    rule.setIfClause(Formula(Nil, Statements(Nil)))
    rule.addElseClause(Properties(List(Property(AirAssert,Formula(Nil, Statements(List(Statement(dummy,dummy,dummy))))))))
    rule.addElseClause(Properties(List(Property(AirDescription,N3List(List(N3Parser.StringLiteral("Else")))))))
    rule.addThenClause(Properties(List(Property(AirDescription,N3List(List(N3Parser.StringLiteral("If")))))))

    val program = RuleCompiler.translate(Map(ruleName -> rule), "some-uri")

    program.get.ruleBuffer.size should be(1)
    val rule1 = program.get.findRule(ruleName).get

    val elseHead = new RuleHead(List(
        new AssertAction(List(Term(dummyUriRef,dummyUriRef,dummyUriRef))),
        new DescriptionAction(ruleName, List(StringLiteral("Else")))))
    val ifHead = new RuleHead(List(new DescriptionAction(ruleName, List(StringLiteral("If")))))
    val rule2 = new Rule(ruleName, "", "", Nil, new RuleBody(List(True)), ifHead, elseHead)

    rule1 should be (rule2)
  }

  // TODO: RuleCompiler tests with variables
  // TODO: RuleCompiler tests with nested rules
}
